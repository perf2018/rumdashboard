import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import store from './stores/configureStore';
import Route from './routes';

//const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <Route></Route>
    </Provider>,
    document.getElementById('root')
)