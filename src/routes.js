import React from 'react';
//import {Route, IndexRoute} from 'react-router';
import App from './App';
import Login from './modules/login/login';
import {BrowserRouter as Router, Link, NavLink, Redirect, Prompt} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import Dashboard from './modules/dashboard/dashboard';


export default class Routes extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            auth: true
        }

        this.reRender = this.reRender.bind(this);
    }

    reRender() {
        this.setState({
            auth: true
        });
    }

    render() {
        return (<div>
            <App auth={this.state.auth}/>
            <Router>
                <div>
                    <Route path='/' exact render={() => (
                        (<Redirect to='/dashboard'/>)
                    )}/>
                    <Route path='/login' exact render={() => (<Login reRender={this.reRender}/>)}/>
                    <Route path='/dashboard' exact render={() => (
                        this.state.auth ? (<Dashboard/>) : (<Redirect to='/login'/>)
                    )}/>
                </div>
            </Router>
        </div>)

    }
}