import { combineReducers } from 'redux';
//import productReducer from './chart-reducers';
import DashboardReducer from '../modules/dashboard/reducers/dashboard-reducer';

export default combineReducers({
    DashboardReducer: DashboardReducer
});