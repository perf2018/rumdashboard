import React, {Component} from 'react';
import AppWrapper from './modules/app/app-bar';
import MenuDrawer from './modules/app/menu-drawer';
import FilterDrawer from './modules/common/filter/filter-drawer';

// this container is for creating layout
class App extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            auth: this.props.auth,
            menuDrawerOpen: false,
            filterDrawerOpen: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state.auth = nextProps.auth;
    }

    menuDrawerOpen() {
        this.setState({menuDrawerOpen: !this.state.menuDrawerOpen})
    }

    filterDrawerOpen() {
        this.setState({filterDrawerOpen: !this.state.filterDrawerOpen})
    }

    render() {
        return (
            <div>
                <AppWrapper auth={this.state.auth} menuDrawerOpen={this.menuDrawerOpen.bind(this)}
                            filterDrawerOpen={this.filterDrawerOpen.bind(this)}/>
                <MenuDrawer menuDrawerOpen={this.state.menuDrawerOpen} close={this.menuDrawerOpen.bind(this)}/>
                <FilterDrawer filterDrawerOpen={this.state.filterDrawerOpen} close={this.filterDrawerOpen.bind(this)}/>
            </div>
        );
    }


}


export default App
