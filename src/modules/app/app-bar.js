import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import NotificationIcon from '@material-ui/icons/Notifications';
import Switch from '@material-ui/core/Switch';
import EventsService from '../common/utils/events-service';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    flex: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    button: {
        margin: theme.spacing.unit,
    }
});

class AppWrapper extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            checkedA: false,
            auth: this.props.auth,
            anchorEl: null,
        };

    }


    componentWillReceiveProps(nextProps) {
        this.state.auth = nextProps.auth;
    }

    handleMenu = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleClose = () => {
        this.setState({anchorEl: null});
    };

    updatePageTrigger = () => {
        EventsService.emitter.emit(EventsService.UPDATE_PAGE_DATA);
    }

    handleChange = name => event => {
        let self = this;
        setTimeout(function () {
            if (self.state.checkedA) {
                self.updatePageTrigger();
            }
        }, 60000);
        this.setState({[name]: event.target.checked});
    };

    render() {
        const {classes} = this.props;
        const {auth, anchorEl} = this.state;
        const open = Boolean(anchorEl);

        return (
            <div className={classes.root}>
                {auth && (<AppBar position="static">
                    <Toolbar disableGutters={false}>
                        <IconButton className={classes.menuButton} color="inherit" aria-label="Menu"
                                    onClick={() => this.props.menuDrawerOpen()}>
                            <MenuIcon/>
                        </IconButton>
                        <Typography variant="title" color="inherit" className={classes.flex}>
                            Photos
                        </Typography>

                        <div>
                                Refresh Page Data
                                <Switch
                                    checked={this.state.checkedA}
                                    onChange={this.handleChange('checkedA').bind(this)}
                                    value="checkedA"
                                />

                            <Button variant="contained" color="secondary" className={classes.button}
                                    onClick={() => this.props.filterDrawerOpen()}>
                                Filter
                            </Button>
                            <IconButton>
                                <Badge badgeContent={4} style={{'color': '#2A3F54'}}>
                                    <NotificationIcon/>
                                </Badge>
                            </IconButton>
                            <IconButton
                                aria-owns={open ? 'menu-appbar' : null}
                                aria-haspopup="true"
                                onClick={this.handleMenu}
                                color="inherit"
                            >
                                <AccountCircle/>
                            </IconButton>
                            <Menu
                                id="menu-appbar"
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'right',
                                }}
                                open={open}
                                onClose={this.handleClose}
                            >
                                <MenuItem onClick={this.handleClose}>Profile</MenuItem>
                                <MenuItem onClick={this.handleClose}>My account</MenuItem>
                            </Menu>
                        </div>

                    </Toolbar>
                </AppBar>)}
            </div>
        );
    }
}

AppWrapper.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppWrapper);