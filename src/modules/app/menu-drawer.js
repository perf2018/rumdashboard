import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import {sideBarItems as menuOptions} from '../dashboard/mockservice/tileData';

const styles = theme => ({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
});

class MenuDrawer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            open: this.props.menuDrawerOpen
        };
        this.toggleDrawer = this.toggleDrawer.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state.open = nextProps.menuDrawerOpen;
    }


    toggleDrawer() {
        this.setState({
            open: !this.state.open,
        });
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Drawer open={this.state.open} onClose={() => this.props.close()}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={() => this.props.close()}
                        onKeyDown={() => this.props.close()}
                    >
                        {menuOptions}
                    </div>
                </Drawer>
            </div>
        );
    }
}

MenuDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MenuDrawer);