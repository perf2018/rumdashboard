<Toolbar>
<IconButton className={classes.menuButton} color="inherit" aria-label="Menu">
  <MenuIcon />
</IconButton>
<Typography variant="title" color="inherit" className={classes.flex}>
  Title
</Typography>
{auth && (
  <div>
    <IconButton
      aria-owns={open ? 'menu-appbar' : null}
      aria-haspopup="true"
      onClick={this.handleMenu}
      color="inherit"
    >
      <AccountCircle />
    </IconButton>
    <Menu
      id="menu-appbar"
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      open={open}
      onClose={this.handleClose}
    >
      <MenuItem onClick={this.handleClose}>Profile</MenuItem>
      <MenuItem onClick={this.handleClose}>My account</MenuItem>
    </Menu>
  </div>
)}
</Toolbar>