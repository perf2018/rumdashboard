import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';

import React from 'react';

//import { getSummary } from "../actions/summary-action";

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        width: '100%',
        height: '100%',
    },
    subheader: {
        width: '100%',
    },
    wrapper: {
        marginTop: "0",
        display: "grid",
        gridTemplateColumns: "auto auto auto auto",
        width: "100%",
        marginLeft: "1%"
    },

    cardforSummary: {
        color: "#2A3F54",
        padding: "2px 16px 1px 10px",
        width: "77%",
        fontSize: "16px",
        fontStyle: "normal",
        background: "#fff",
        fontWeight: "600",
        lineHeight: "20px",
        marginBottom: "2%",
        letterSpacing: "-.096px",
        borderTopWidth: "4px",
        borderTopStyle: "solid",
        borderRadius: "0px"
    },
    numberSummary: {
        color: "#2A3F54",
        fontSize: "30px",
        fontWeight: "400",
        lineHeight: "1.471"
    }
});


class DashboardSummary extends React.Component {

    componentWillMount() {
        var summary = [];
        console.log("summary", summary);
    }

    render() {
        const { classes } = this.props;
        if (this.props.items && this.props.items.length !== 0) {
            return <div className={classes.root}>
                <GridList cellHeight={160} className={classes.gridList} cols={6}>
                    {this.props.items.map(tile => (
                        <GridListTile cols={1}>
                            <Card className={classes.cardforSummary}>
                                <CardContent className={classes.summaryCards}>
                                    <Typography>
                                        {tile.name}
                                    </Typography>
                                    <Typography className={classes.numberSummary}>
                                        {tile.value}
                                    </Typography>
                                </CardContent>
                                <CardMedia
                                    className={classes.cover}
                                    image="/static/images/cards/live-from-space.jpg"
                                    title="Live from space album cover"
                                />
                            </Card>
                        </GridListTile>
                    ))}
                </GridList>
            </div>
        } else {
            return <div></div>
        }
    };


}

DashboardSummary.propTypes = {
    classes: PropTypes.object.isRequired,
};
//export default connect(mapStateToProps,mapDispatchToProps)(withStyles(styles, { withTheme: true })(Summary));

export default withStyles(styles)(DashboardSummary);

