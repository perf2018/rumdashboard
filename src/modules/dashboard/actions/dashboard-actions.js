import {FETCH_DASHBOARD_SUMMARY} from "./types";
import {FETCH_DASHBOARD_HIST} from "./types";
import {FETCH_DEVICE_TYPE_DISTRIBUTION} from './types';
import {FETCH_FIRST_DASHBOARD_DATA} from './types';
import HttpService from '../../common/utils/http-service';

const httpService = new HttpService();

export function fetchDashboardData(body) {
    return function(dispatch) {
        Promise.all(
            [
                httpService.post('/summary', {}, body),
                httpService.post('/perfhisto', {}, body),
                httpService.post('/deviceType', {}, body),
                httpService.post('/bandWidth/', {}, body),
                httpService.post('/experienceByPage/', {}, body),
                httpService.post('/experienceByGeo/', {}, body),
                httpService.post('/experienceByBrowser/', {}, body),
            ]
        ).then(result => {
            dispatch({
                type: FETCH_FIRST_DASHBOARD_DATA,
                payload: result
            })
        })
    }
}

export function fetchDashboardHist(body) {
    return function(dispatch) {
        httpService.post('/perfhisto', {}, body)
            .then(data => dispatch({
                type: FETCH_DASHBOARD_HIST,
                payload: data
            }))
    }
}

export function fetchDeviceTypeDistribution(body) {
    return function(dispatch) {
        httpService.post('/deviceType/', {}, body)
            .then(data => dispatch({
                type: FETCH_DEVICE_TYPE_DISTRIBUTION,
                payload: data
            }))
    }
}

export function fetchBandwidthDistribution(body) {
    return function(dispatch) {
        httpService.post('/bandWidth/', {}, body)
            .then(data => dispatch({
                type: FETCH_DEVICE_TYPE_DISTRIBUTION,
                payload: data
            }))
    }
}