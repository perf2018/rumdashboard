import React from 'react';
import {bindActionCreators} from 'redux';
import compose from 'recompose/compose';
import _ from "lodash";
import moment from 'moment';

import {withStyles} from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import EventsService from '../common/utils/events-service';
import DashboardSummary from './components/dashboard-summary';

import {connect} from 'react-redux';
import * as actionCreators from './actions/dashboard-actions';

import ChartFactory from "../../factories/chart-factory";

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    subheader: {
        width: '100%',
    },
    card: {
        minWidth: 275,
    }
});


class Dashboard extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            filters: {},
            config: [
                {
                    key: 1,
                    type: "Histogram",
                    dataPath: "dashboardHist",
                    col: 2
                },
                {
                    key: 2,
                    type: "PieChart",
                    dataPath: "deviceType",
                    col: 1
                },
                {
                    key: 3,
                    type: "PieChart",
                    dataPath: "bandwidth",
                    col: 1
                },
                {
                    key: 4,
                    type: "Table",
                    dataPath: "experienceByPage",
                    col: 1
                },
                {
                    key: 5,
                    type: "Table",
                    dataPath: "experienceByGeo",
                    col: 1
                },
                {
                    key: 6,
                    type: "Table",
                    dataPath: "experienceByBrowser",
                    col: 1
                }
            ]
        };

        this.getPageDate = this.getPageDate.bind(this);
    }


    componentWillMount() {
        EventsService.emitter.emit(EventsService.SET_FILTERS);
        EventsService.emitter.on(EventsService.FILTERS_UPDATED, this.getPageDate);
        EventsService.emitter.on(EventsService.UPDATE_PAGE_DATA, this.getPageDate);
        //this.props.fetchDashboardSummary();
    }

    componentWillUnmount() {
        EventsService.emitter.removeListener(EventsService.FILTERS_UPDATED, this.getPageDate);
        EventsService.emitter.removeListener(EventsService.UPDATE_PAGE_DATA, this.getPageDate);
    }

    componentDidMount() {
        this.getPageDate();
    }

    getPageDate() {
        let result = {};
        EventsService.emitter.emit(EventsService.GET_FILTERS, result);
        this.props.fetchDashboardData(result.filters);
    }

    formatHistogramData(input) {
        let _input = {};
        if (input && input.data && input.data.length > 0) {
            let output = [];
            for (let i = 0; i < input.data[0].graphData.length; i++) {
                let o = {};
                o[input.data[0].graphName.replace(new RegExp(" ", 'g'), "_")] = input.data[0].graphData[i].value;
                o[input.data[1].graphName.replace(new RegExp(" ", 'g'), "_")] = input.data[1].graphData[i].value;
                o.time = moment(input.data[0].graphData[i].time).format("YYYY-MM-DDTHH:mm");

                output.push(o);
            }
            _input.legends = [input.data[0].graphName, input.data[1].graphName];
            _input.data = output;
            _input.datadate = "started";
            _input.show = true;
            _input.styles = {
                height: "200px",
                width: "400px"
            };
            _input.title = input.title;
            _input.type = 'Histogram';

        }
        return _input;
    }

    render() {
        const {classes} = this.props;
        if (!this.props.dashboardHist) {
            return <div></div>
        }
        let processedData = {
            dashboardHist: this.formatHistogramData(this.props.dashboardHist.data),
            deviceType: this.props.deviceType.data,
            bandwidth: this.props.bandwidth.data,
            experienceByPage: this.props.experienceByPage.data,
            experienceByGeo: this.props.experienceByGeo.data,
            experienceByBrowser: this.props.experienceByBrowser.data
        };
        let graphs = _.filter(_.map(this.state.config, c => {
            c.data = _.get(processedData, c.dataPath);
            return c;
        }), c => {
            console.log(c.data, _.get(c, 'data.data'));
            return c.data && _.get(c, 'data.data');
        });
        return (
            <main>
                <DashboardSummary items={this.props.dashboardSummary.data}></DashboardSummary>
                <div className={classes.root}>
                    <GridList cellHeight={160} className={classes.gridList} cols={2} cellHeight={"auto"} spacing={10}>
                        {_.map(graphs, graph => (
                            <GridListTile key={graph.key} cols={graph.cols} row={1}>
                                <Card className={classes.card}>
                                    <CardContent>
                                        <Typography className={classes.title} color="textSecondary">
                                            {graph.data.title}
                                        </Typography>
                                        <ChartFactory config={graph.data}></ChartFactory>
                                    </CardContent>
                                </Card>
                            </GridListTile>
                        ))}
                    </GridList>
                </div>
            </main>
        );
    }
}

const mapStateToProps = state => ({
    dashboardSummary: state.DashboardReducer.dashboardSummary,
    dashboardHist: state.DashboardReducer.dashboardHist,
    deviceType: state.DashboardReducer.deviceType,
    bandwidth: state.DashboardReducer.bandwidth,
    experienceByPage: state.DashboardReducer.experienceByPage,
    experienceByGeo: state.DashboardReducer.experienceByGeo,
    experienceByBrowser: state.DashboardReducer.experienceByBrowser
});

const mapDispatchToProps = dispatch => {
    return bindActionCreators(actionCreators, dispatch);
}

export default compose(
    withStyles(styles, {name: 'Dashboard'}),
    connect(mapStateToProps, mapDispatchToProps)
)(Dashboard);
