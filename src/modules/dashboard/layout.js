import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';

import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { sideBarItems } from './mockservice/tileData';
import Summary from './components/dashboard-summary';
import tileData from './mockservice/gridLayoutConfig';
import CardforView from '../common/html-components/cards';
import Badge from '@material-ui/core/Badge';
import ImageAvatars from '../common/html-components/avatar';

import MailIcon from '@material-ui/icons/Mail';
import PersonIcon from '@material-ui/icons/Person';
import NotificationIcon from '@material-ui/icons/Notifications';
import { Typography } from '@material-ui/core';




const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  cardStyle : {
    display: "block",
    width: '70%',
    transitionDuration: '0.3s',
},
  appFrame: {
    height: "90%",
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    position: 'absolute',
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    borderBottom:"rgba(0, 0, 0, 0.1) solid 1px",
    backgroundColor:"#EDEDED",
    boxShadow:"none"

  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  appBarShiftLeft: {
    marginLeft: drawerWidth,
  },
  'appBarShift-right': {
    marginRight: drawerWidth,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    width: drawerWidth,
    background: "#2A3F54",

  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 2px',
    ...theme.mixins.toolbar,
  },
  
  wrapper : {
    display: "flex",
    overflow: "hidden",
    flexWrap: "wrap",
    marginTop: "0%",
    paddingTop: "2%",
    marginLeft: "0"

  },

  cardforGraph: {
    flex:"0 1 calc(50% - 1em)",
    varWidth:"80%"
   },

  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  'content-left': {
    marginLeft: -drawerWidth,
  },
  'content-right': {
    marginRight: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  'contentShift-left': {
    marginLeft: 0,
  },
  'contentShift-right': {
    marginRight: 0,
  },
  heading: {
        color: '#FFF',
        marginLeft :"21%"
    }
});

class LayOut extends React.Component {
  state = {
    open: false,
    anchor: 'left',
  };

  componentWillMount = () =>{
    const firstrow={ 
    width: "97%",
    height: "300px",
    marginLeft:"1%",
    borderShadow:"none",
    borderRadius:"0px"
    };
    const secondrow={
    "margin-bottom": "2%",
    "margin-left": "1%",
    "height": "320px",
    "width": "97%",
    "border-shadow":"none",
    "border-radius":"0px"
    

    }
    this.setState({"firstrow":firstrow});
    this.setState({"secondrow":secondrow});
    

  }
  toggleLogin = () => {this.setState( {...this.state, login: !this.state.login } ) }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handleChangeAnchor = event => {
    this.setState({
      anchor: event.target.value,
    });
  };

  render() {
    console.log("data to form cards",tileData);

    const { classes, theme } = this.props;
    const row1 = tileData[0].graphcardsLayout.layout1; // move this either to backend
    const row2 = tileData[0].graphcardsLayout.layout2; //// move this either to backend   
    const { anchor, open } = this.state;
   
    const drawer = (
      <Drawer
        variant="persistent"
        anchor={anchor}
        open={open}
        classes={{
          paper: classes.drawerPaper,
        }} style={{"background":"#2A3F54"}}>
        <ImageAvatars></ImageAvatars>
        <div className={classes.drawerHeader}>
           <Typography style={{color:"#fafafa"}}>Real User Monitor</Typography>
          <IconButton onClick={this.handleDrawerClose}>
            {theme.direction === 'rtl' ? <ChevronRightIcon style={{"color":"#5A738E"}}/> : <ChevronLeftIcon />}
          </IconButton>
        </div>
        <Divider />
        <List>{sideBarItems}</List>
       
      </Drawer>
    );

    let before = null;
    let after = null;

    if (anchor === 'left') {
      before = drawer;
    } else {
      after = drawer;
    }
    return (
      <div className={classes.root}>
      
        <div className={classes.appFrame}>
          <AppBar
            className={classNames(classes.appBar, {
              [classes.appBarShift]: open,
              [classes[`appBarShift-${anchor}`]]: open,
            })} >
            <Toolbar disableGutters={!open}>
            <div style={{"display":"grid","grid-template-columns":"26fr 2fr 2fr 1fr"}}>
            <div>
              <IconButton
                color="inherit"
                aria-label="open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(classes.menuButton, open && classes.hide)}
                style={{"color":"#2A3F54"}}>
                <MenuIcon />
              </IconButton>
              
            </div>  
            <div >
              <IconButton>
                    <Badge badgeContent={4} style={{'color':'#2A3F54'}}>
                          <MailIcon />
                    </Badge>
              </IconButton>
              </div>
              <div>
              <IconButton>
                    <Badge style={{'color':'#2A3F54'}}>
                          <PersonIcon />
                    </Badge>
              </IconButton>
               </div>
               <div>
              <IconButton>
                    <Badge badgeContent={4} style={{'color':'#2A3F54'}}>
                          <NotificationIcon />
                    </Badge>
              </IconButton>
               </div> 
            </div>  
           
            </Toolbar>
           
          </AppBar>
          {before}
          <main
            className={classNames(classes.content, classes[`content-${anchor}`], {
              [classes.contentShift]: open,
              [classes[`contentShift-${anchor}`]]: open,
            })}>
          <div className={classes.drawerHeader} />
          <div className={classes.root}>

            <Summary></Summary>
            <CardforView data={tileData[0].graphcards[0]} style={row1} cardsDimensions={this.state.secondrow} ></CardforView>

            <div style={row2}>
                 
                {tileData[0].graphcards.map(tile => (
                    <CardforView data={tile} cardsDimensions={this.state.firstrow}></CardforView>
                ))}


            </div>  
          </div>
   
                  
                    
     
          </main>
          {after}
        </div>
      </div>
    );
  }
}

LayOut.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(LayOut);