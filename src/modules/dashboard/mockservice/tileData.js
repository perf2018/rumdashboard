// This file is shared across the demos.

import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import InboxIcon from '@material-ui/icons/Dashboard';
import DraftsIcon from '@material-ui/icons/Drafts';
import StarIcon from '@material-ui/icons/Star';
import SendIcon from '@material-ui/icons/Send';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

const itemstextCSS ={
  color: "#fafafa"
}
export const sideBarItems = (
  <div>

    <ListItem button>
      <ListItemIcon>
        <InboxIcon style={itemstextCSS} />
      </ListItemIcon>
      <ListItemText  disableTypography
        primary={<Typography type="body2" style={itemstextCSS}>Dashboard</Typography>} />
    </ListItem>
    <Divider />

    <ListItem button>
      <ListItemIcon>
        <StarIcon style={itemstextCSS}/>
      </ListItemIcon>
      <ListItemText  disableTypography
        primary={<Typography type="body2" style={itemstextCSS}>Builder</Typography>} />    </ListItem>
       <Divider />

    <ListItem button>
      <ListItemIcon>
        <SendIcon style={itemstextCSS}/>
      </ListItemIcon>
      <ListItemText  disableTypography
        primary={<Typography type="body2" style={itemstextCSS}>DataStore</Typography>} />    </ListItem>
        <Divider />

    <ListItem button>
      <ListItemIcon>
        <DraftsIcon style={itemstextCSS}/>
      </ListItemIcon>
      <ListItemText  disableTypography
        primary={<Typography type="body2" style={itemstextCSS}>Admin</Typography>} />    </ListItem>
      <Divider />

  </div>
);

