import { combineReducers } from 'redux';
import productReducer from './chart-reducers';
export default combineReducers({
    productReducer
});