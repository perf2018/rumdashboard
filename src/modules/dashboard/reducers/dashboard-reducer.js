import {FETCH_DASHBOARD_SUMMARY} from "../actions/types";
import {FETCH_DASHBOARD_HIST} from "../actions/types";
import {FETCH_DEVICE_TYPE_DISTRIBUTION} from '../actions/types';
import {FETCH_BANDWIDTH_DISTRIBUTION} from '../actions/types';
import {FETCH_FIRST_DASHBOARD_DATA} from '../actions/types';

const initialState = {
    dashboardSummary: []
}


export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_DASHBOARD_SUMMARY:
            return {
                ...state,
                dashboardSummary: action.payload
            };
        case FETCH_DASHBOARD_HIST:
            return {
                ...state,
                dashboardHist: action.payload
            }
        case FETCH_DEVICE_TYPE_DISTRIBUTION:
            return {
                ...state,
                deviceType: action.payload
            }
        case FETCH_BANDWIDTH_DISTRIBUTION:
            return {
                ...state,
                bandwidth: action.payload
            }
        case FETCH_FIRST_DASHBOARD_DATA:
            return {
                ...state,
                dashboardSummary: action.payload[0],
                dashboardHist: action.payload[1],
                deviceType: action.payload[2],
                bandwidth: action.payload[3],
                experienceByPage: action.payload[4],
                experienceByGeo: action.payload[5],
                experienceByBrowser: action.payload[6]
            }
        default:
            return state;
    }
}