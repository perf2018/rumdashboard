import {
  FETCH_SUMMARY_BEGIN,
  FETCH_SUMMARY_SUCCESS,
  FETCH_SUMMARY_FAILURE
} from '../actions/summary-action';


const initialState = {
  items: [],
  loading: false,
  error: null
};


export default function productReducer(state = initialState, action) {
  console.log("action",action)
  switch(action.type) {
    case FETCH_SUMMARY_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
        items:[]
      };

    case FETCH_SUMMARY_SUCCESS:

      return {
        ...state,
        loading: false,
        error: null,
        items: action.payload
      };

    case FETCH_SUMMARY_FAILURE:

      return {
        ...state,
        loading: true,
        error: action.payload.error,
        items: []
      };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}