import axios from 'axios';
import queryString from 'querystring';

const HOST = 'http://35.237.44.183:8083/api/dashboard';
export default class HttpService {

    _getUrl(path, params) {
        return (HOST + path + '?' + queryString.stringify(params));
    };

    _getHeaders() {
        return {
             'X-Auth-Token': localStorage.getItem('X-Auth-Token'),
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        };
    }

    get(path, params) {
        var url = this._getUrl(path, params);
        var headers = {headers: this._getHeaders()};

        return axios.get(url, headers);
    }

    post(path, params, body) {
        body = body || {};
        var headers = {headers: this._getHeaders()};
        var url = this._getUrl(path, params);

        return axios.post(url, body, headers);
    }

    put(path, params, body) {
        body = body || {};
        var headers = {headers: this._getHeaders()};
        var url = this._getUrl(path, params);

        return axios.put(url, body, headers)
    }

    delete(path, params) {
        var url = this._getUrl(path, params);
        var headers = {headers: this._getHeaders()};

        return axios.delete(url, headers);
    }
}