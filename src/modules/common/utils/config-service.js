import _ from 'lodash';

const CONFIG = 'config';
export default class ConfigService {

    constructor() {
        this.config = {};
    }

    setConfig(config) {
        this.config = config;
        localStorage.setItem(CONFIG, this.config);
    }

    _getConfig() {
        if(!this.config) {
            this.config = localStorage.getItem(CONFIG);
        }

        return this.config;
    }

    getFilterByPage(page) {
        if(!this.config) {
            this.config = this._getConfig();
        }
        return this.config['filter'][page];
    }

}