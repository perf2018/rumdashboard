export const COMMON = {
    COLOR_STEP: 15,
    DATE_FORMAT: "MMM DD YYYY",
    VIEW_MODE: {
        EDIT: 'edit',
        DETAILS: 'details'
    }
}
