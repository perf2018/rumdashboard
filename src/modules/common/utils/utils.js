/**
 * Created by hari on 19/10/16.
 */

import shortid from 'shortid';

export default  class Utils {
    static getUniqueKeys(num) {
        num = num || 1;
        let keys = [];

        for (let i = 0; i < num; i++) {
            keys.push(shortid.generate());
        }
        return keys;
    }
}
