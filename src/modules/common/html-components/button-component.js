/**
 * Created by sameer on 30/05/16.
 */

import React from 'react';

class ButtonComponent extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    _getButtonClasses() {
        let classes = 'btn ';
        _.each(this.props.button.buttonClasses, (c) => {
            classes = classes + c + ' ';
        });

        return classes;
    }

    _getIconClass() {
        let classes = '';
        _.each(this.props.button.iconClasses, (c) => {
            classes = classes + c + ' ';
        })

        return classes;
    }

    _onClick() {
        this.props.onClick ? this.props.onClick.call(this) : this.props.button.onClick.call(this, this.props.button.onClickParams);
    }

    render() {
        return (<button className={this._getButtonClasses()} onClick={this._onClick.bind(this)} disabled={this.props.disabled}>
            <span className={this._getIconClass()}></span><span className="btn-text">{this.props.button.buttonText}</span>
        </button>)

    }
}

export default ButtonComponent;
