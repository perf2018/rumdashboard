import React from 'react';
import EventsService  from '../../services/events-service';

const messageDisplayTime = 5000;
export default class ErrorMsg extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.successMsg = this.successMsg.bind(this);

        this.state = {};
    }

    componentDidMount() {
        EventsService.emitter.addListener(EventsService.SHOW_MESSAGE, this.successMsg);
    }


    successMsg(params) {
        let self = this;

        setTimeout(function() {
            self.setState({
                position: 1,
                messageOpen: false
            })
        }, messageDisplayTime);

        this.setState({
            msg: params.msg,
            msgType: params.msgType,
            messageOpen: true
        })
    }

    render() {

        return (<div>

        {this.state.messageOpen ? <div className={`${this.state.msgType} message-up`}>
            <span></span>
            <span>{this.state.msg}</span>
        </div> : <div></div> }
            </div>
        );
    }
}

