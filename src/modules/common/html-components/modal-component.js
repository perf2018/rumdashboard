/**
 * Created by sameer on 01/06/16.
 */

import React from 'react';
import ReactModal from 'react-modal';
import Form from '../../form/form';

class ModalComponent extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            modalIsOpen: false
        }
        this.customStyles = {
            content : {
                top                   : '40%',
                left                  : '40%',
                right                 : 'auto',
                bottom                : 'auto',
                marginRight           : '-50%',
                transform             : 'translate(-50%, -50%)'
            }
        };

    }

    openModal() {
        this.setState({
            modalIsOpen: true
        });
    }

    afterOpenModal() {
    }



    componentWillMount() {
        ReactModal.setAppElement('body');
        if (this.props.modalConfig.modalIsOpen && !this.state.modalIsOpen) {
            this.setState({
                modalIsOpen: true
            })
        }
    }

    render() {
        return (<div>

                <ReactModal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal}
                    style={this.customStyles} ref="modal">
                </ReactModal>
            </div>
        )
    }

}

export default ModalComponent;
