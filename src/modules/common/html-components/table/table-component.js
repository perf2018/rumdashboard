/**
 * Created by sameer on 16/05/16.
 */

import React from 'react';
import _ from 'lodash';
import ReactTooltip from "react-tooltip";
import StringUtils from '../../utils/string-utils';
class TableComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.propTypes = {
            tableData: React.PropTypes.object.isRequired,
            tableConfigs: React.PropTypes.object.isRequired,
            columnConfigs: React.PropTypes.object.isRequired,
            sortOn: React.PropTypes.string.isRequired,
            onSort: React.PropTypes.func.isRequired,
            sortType: React.PropTypes.string.isRequired
        };

        let selectionMap = this.props.tableData.map(r => {
            return false;
        });

        let rowClickedMap = this.props.tableData.map(r => {
            return false;
        });

        this.state = {
            selectionMap: selectionMap,
            rowClickedMap: rowClickedMap,
            tableData: this.props.tableData,
            downloadConfig: this.props.tableConfigs.downloadConfig
        };

    }

    componentWillReceiveProps(nextProps) {
        let selectionMap = nextProps.tableData.map(r => {
            return false;
        });

        let rowClickedMap = nextProps.tableData.map(r => {
            return false;
        });

        this.state.selectionMap = selectionMap;
        this.state.rowClickedMap = rowClickedMap;
        this.state.tableData = nextProps.tableData;
    }

    /**
     * generates the row and columns, applies classes and custom classes.
     * @returns {XML}
     */
    //TODO: check if anchor tag can be used for state change rather than using angular $state.
    _createRows() {
        let self = this;
        self.props.tableConfigs.isRowSelectionEnabled = self.props.tableConfigs.isRowSelectionEnabled?self.props.tableConfigs.isRowSelectionEnabled : ()=>{return true};
        return (<tbody>{self.state.tableData.map(function (r, i) {

                return (
                    <tr key={i} className={self.state.rowClickedMap[i] ? 'branding-active' : ''}>
                {self.props.tableConfigs.rowSelection ?
                    self.props.tableConfigs.isRowSelectionEnabled(r) ?  <td key={i + '0.0'}><input type="checkbox" name="rowSelection"
                    onChange={self._handleCheckBoxChangeEvent.bind(self, r, i)}
                    checked={self.state.selectionMap[i++]}/>
                </td> : <td key={i + '0.0'}><input type="checkbox" name="rowSelection" disabled="true" checked={false} />
                    </td>: ''}
                {self.props.columnConfigs.map(function (c, j) {
                    if(c.toolTip) {
                        if(c.customCell) {
                            return(
                                <td key={i + '.' + j} className={self._getCustomStyles(c, r)} onClick={() => self._handleRowClick(r, i)}
                            data-tip={StringUtils.htmlDecode(self._getCellValue(c, r))}>{c.customCell ? self._getCustomCell(c, r, self.props.columnConfigs)
                                :StringUtils.getDisplayValue(self._getCellValue(c, r), true)}</td>
                        )
                        } else {
                            return (<td key={i + '.' + j} className={self._getCustomStyles(c, r)}  onClick={() => self._handleRowClick(r, i)}
                            dangerouslySetInnerHTML={{__html: StringUtils.getDisplayValue(self._getCellValue(c, r), true)}}
                                        data-tip={StringUtils.htmlDecode(self._getCellValue(c, r))}>
                        </td>)}
                    }else{
                        if(c.customCell) {
                            return(
                                <td key={i + '.' + j} className={self._getCustomStyles(c, r)} onClick={() => self._handleRowClick(r, i)}
                        >{self._getCustomCell(c, r, self.props.columnConfigs)}</td>
                        )
                        } else {
                            return (<td key={i + '.' + j} className={self._getCustomStyles(c, r)}  onClick={() => self._handleRowClick(r, i)}
                        >{StringUtils.getDisplayValue(self._getCellValue(c, r), true)}</td>)
                        }

                        }

                    }.bind(self))}
                </tr>)
        }.bind(self))}</tbody>);
    }

    _handleRowClick(r, i, event) {
        if (this.props.tableConfigs.rowClick) {
            this.props.tableConfigs.rowClick.call(null, r, null, this.props.currentPage);
        }

        if(this.props.tableConfigs.rowClickHightlight) {
            this.setState({
                rowClickedMap: this.state.rowClickedMap.map((r, index) => {
                    return (index == i);
                })
            })
        }

    }

    _handleCheckBoxChangeEvent(row, index, event) {
        let selectionMap = this.state.selectionMap;
        selectionMap[index] = event.target.checked;
        this.props.tableConfigs.rowSelection.call(null, row, event.target.checked);
        this.setState({
            selectionMap: selectionMap
        })
    }

    _handleCheckBoxSelectAll(event) {
        let selectionMap = this.state.selectionMap;
        selectionMap = selectionMap.map(s => {
            return event.target.checked;
        });
        this.props.tableConfigs.rowSelection.call(null, {}, 'all', event.target.checked);
        this.setState({
            selectionMap: selectionMap
        })
    }

    /**
     * generates the classes also if column is hidden.
     * @param config
     * @returns {string}
     */
    _getCustomStyles(config, row) {
        let classes = '';

        if (config.hidden) {
            return 'hideColumn';
        }

        if (config.customStyles) {
            classes = config.customStyles.join(' ');
        }

        if(this.props.tableConfigs.rowClick) {
            classes = classes + 'hoverHand ';
        }

        if(config.customStyleFunction) {
            classes = classes + config.customStyleFunction.call(null, config, row);
        }


        return classes;
    }

    /**
     * binds on click event on custom html.
     * @param row
     * @param config
     */
    _onClick(row, config, event) {
        event.stopPropagation();
        if (config.callback) {
            config.callback.call(null, row);
        }

    }

    /**
     * generates the content of the cell.
     * @param config
     * @param row
     * @returns {*}
     */
    _getCustomCell(config, row, columsConfig) {
        return config.customCell ? config.customCell.call(null, row, config,columsConfig) : (config.valueDecorator ? config.valueDecorator.call(null, row, config, columsConfig) : _.get(row, config.path));
    }

    _getCellValue(config, row) {
        return config.valueDecorator ? config.valueDecorator.call(null, row, config) : _.get(row, config.path);
    }
    /**
     * creates headers.
     * @returns {XML}
     */
    _createHeaders() {

        let self = this;
        return (<thead>
        <tr>
            {self.props.tableConfigs.rowSelection ? (( _.isUndefined(self.props.tableConfigs.isSelectAllEnabled)
            || self.props.tableConfigs.isSelectAllEnabled())
                 ? <th key="0.0.0"><input type="checkbox" name="rowSelectionAll" onChange={self._handleCheckBoxSelectAll.bind(self)}/>
            </th> : <th key="0.0.0"><input type="checkbox" name="rowSelectionAll" disabled={true}/></th>) : ''}
            {self.props.columnConfigs.map(function (c, i) {
                if (c.isSortable) {
                    return c.path == self.props.sortOn ? (self.props.sortType == 'asc' ?
                        <th key={i} className={c.hidden ? 'hideColumn hoverHand':'hoverHand'}
                            onClick={self._onSort.bind(self, c.path, 'desc')}>
                            <span>{c.columnName}</span> <span className="dropup"><i
                            className="fa caret"
                        ></i></span></th>
                        :
                        <th key={i} className={c.hidden ? 'hideColumn hoverHand':'hoverHand'}
                            onClick={self._onSort.bind(self, c.path, 'asc')}>
                            <span>{c.columnName}</span><span
                            className="dropdown"><i className="fa caret"
                        ></i></span></th>)
                        :
                        <th key={i} className={c.hidden ? 'hideColumn hoverHand':'hoverHand'}
                            onClick={self._onSort.bind(self, c.path, 'asc')}><span>{c.columnName}</span></th>
                }
                else {
                    return <th key={i} className={c.hidden ? 'hideColumn':''}><span>{c.columnName}</span></th>
                }


            })}

        </tr>
        </thead>)
    }

    _onSort(sortOn, sortType, event) {
        event.stopPropagation();
        if(this.props.tableConfigs.defaultSort) {
            this.props.tableConfigs.defaultSort.call(null, sortOn, sortType);
        } else {
            _.orderBy(this.state.tableData, [sortOn], [sortType]);
            let selectionMap = this.state.tableData.map(r => {
                return false;
            });

            let rowClickedMap = this.state.tableData.map(r => {
                return false;
            });

            this.setState = {
                selectionMap: selectionMap,
                rowClickedMap: rowClickedMap,
                tableData: this.state.tableData
            }
        }
    }

    render() {

        return (<div className="row">
            <div className="col-md-12 table-scroll">
                <table ref="vymo-table" className="table-hover table table-striped">
                    {this._createHeaders.call(this)}
                    {this._createRows.call(this)}
                </table>
                <ReactTooltip/>
            </div>
        </div>)
    }
}

export default TableComponent;
