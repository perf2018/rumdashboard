/**
 * Created by sameer on 25/05/16.
 */

import _ from 'lodash';

class TableConfigHelper {
    static get $inject() {
        return [];
    }

    constructor() {

    }

    getColumnConfig(classRef, columnConfigs) {
        let self = this;
        columnConfigs = _.map(columnConfigs, c => {
            if (c.valueDecorator) {
                c.valueDecorator = classRef[c.valueDecorator]
            }
            return c;
        });
        return columnConfigs;
    }

    getTableConfig(classRef, tableConfigs) {
        if (tableConfigs.rowSelection) {
            tableConfigs.rowSelection = classRef[tableConfigs.rowSelection];
        }

        return tableConfigs;
    }
}

export  default TableConfigHelper;
