/**
 * Created by sameer on 17/05/16.
 */

import React from 'react';
import _ from 'lodash';
import TableComponent from './table-component';
import SearchBox from './search-box';
import Pagination from './pagination';
import CsvGenerator from '../../../modules/utils/export-csv';
import Button from '../../commons/html-components/button/button-component'

class TableWrapper extends React.Component {
    constructor(props, context) {
        super(props, context);
        this._onSearch = this._onSearch.bind(this);
        this._onPagination = this._onPagination.bind(this);
        this._sortTableData = this._sortTableData.bind(this);

        this.numberOfPages = this.props.tableConfigs.pagination.totalPages || 0;
        this.state = {
            currentPage: 1,
            query: '',
            sortOn: this.props.tableConfigs.defaultSort,
            sortType: 'asc',
            pageLength: this.props.tableConfigs.pageSize || 1,
            pagination: this.props.tableConfigs.pagination
        };
        this.propTypes = {
            tableData: React.PropTypes.object.isRequired,
            tableConfigs: React.PropTypes.object.isRequired,
            columnConfigs: React.PropTypes.object.isRequired
        };
        this.totalPages = this.props.tableConfigs.pagination.totalPages;
        this.totalCount = this.props.tableConfigs.pagination.totalCount;
        this.searchStrCount = 1;
        this.searchString = false;
        this.prevQuery = '';
        this.downloadButton = {
            buttonClasses: ['btn-default', 'vymo-btn-secondary', 'pull-left', 'custom-left-spacing'],
            iconClasses: ['fa', 'fa-download'],
            buttonText: "DOWNLOAD CSV",
            onClick: this._getCSV.bind(this)
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state.tableConfigs = nextProps.tableConfigs;
        this.setState({
            currentPage: nextProps.currentPage || 1
        });
        this.totalPages = nextProps.tableConfigs.pagination.totalPages;
        this.totalCount = nextProps.tableConfigs.pagination.totalCount;
    }

    /**
     * for config search: true and column seachAble generates the string on which searching takes place.
     * @returns {*}
     * @private
     */
    _generateSearchString() {
        let tableData = this.props.tableData,
            configs = this.props.columnConfigs;
        _.each(tableData, function (d) {
            let searchString = '';
            _.each(configs, function (c) {
                if (c.searchAble) {
                    let value = _.get(d, c.path);
                    searchString = searchString + (typeof value === 'string' ? value.toUpperCase() : value);
                }

            });
            d.searchString = searchString;
        });

        return tableData;

    }

    /**
     * trigger for on search
     * @param query
     * @private
     */
    _onSearch(query) {

        this.setState({
            query: query.toUpperCase()
        });
    }

    /**
     * trigger for pagination
     * @param pageNumber
     * @private
     */
    _onPagination(pageNumber) {
        if (this.searchStrCount) {
            this.searchStrCount = pageNumber;
        }
        this.setState({
            currentPage: pageNumber
        });

        if (this.props.tableConfigs.pagination.callBack) {
            this.props.tableConfigs.pagination.callBack.call(this, null, pageNumber, this.props.tableConfigs.pageSize);
        }
    }

    _sortTableData(sortOn, sortType) {
        this.setState({
            sortOn: sortOn,
            sortType: sortType
        })
    }

    /**
     * generates the content of the cell.
     * @param config
     * @param row
     * @returns {*}
     */
    _getCustomCell(config, row, columsConfig) {
        return config.customCell ? config.customCell.call(null, row, config, columsConfig) : (config.valueDecorator ? config.valueDecorator.call(null, row, config, columsConfig) : _.get(row, config.path));
    }

    _getCSV(reportName) {
        let self = this;
        let downLoadAbleData = [];
        let headerRow = [];

        _.each(this.props.tableData, function (row) {
            let currentRow = [];
            _.each(self.props.columnConfigs, function (columnConfig) {
                currentRow.push(self._getCustomCell(columnConfig, row, self.props.columnConfigs));
            });
            downLoadAbleData.push(currentRow);
        });

        //build header
        _.each(self.props.columnConfigs, function (columnConfig) {
            headerRow.push(columnConfig.columnName);
        });
        downLoadAbleData.unshift(headerRow);
        let exportCsv = new CsvGenerator(downLoadAbleData, reportName);
        return exportCsv.download(true);
    }

    /**
     * generates the final displayable data after applying pagination and search.
     * @returns {*}
     * @private
     */
    _getFinalRenderData() {

        let query = this.state.query,
            tableData = this._generateSearchString(this.props.tableData),
            self = this;

        if (query) {
            self.tableData = tableData.filter(function (d) {

                return d.searchString.indexOf(query) != -1;
            });
        } else {
            self.tableData = tableData;
        }

        //TODO: need to revisit the logic.
        if (self.props.sortOn) {
            let path = '';
            _.each(self.props.columnConfigs, function (c) {
                if (c.columnName === self.props.sortOn)
                    path = c.path;
            });
            self.tableData = _.sortBy(self.tableData, function (rowData) {
                return self.props.sortType == 'asc' ? _.get(rowData, path) : !_.get(rowData, path);
            })
        }

        if (self.state.pagination && !self.state.pagination.dataFromCallBack) {
            self.numberOfPages = Math.ceil(self.tableData.length / self.state.pageLength);
            self.totalPages = self.numberOfPages;
            self.totalCount = self.tableData.length;
            if (!query) {
                this.searchString = false;
                this.tableData = self.tableData.slice((this.state.currentPage - 1 ) * self.state.pageLength,
                    self.state.currentPage * self.state.pageLength);
            } else {
                this.searchString = true;
                if (this.prevQuery !== query) {
                    this.searchStrCount = 1;
                }
                if (self.totalCount > self.state.pageLength) {
                    this.tableData = self.tableData.slice((this.searchStrCount - 1) * self.state.pageLength,
                        self.state.pageLength * this.searchStrCount);
                }
            }

        }

        this.prevQuery = query;

        return self.tableData;
    }

    formFootNotes() {
        let html = "";
        _.each(this.props.tableConfigs.footNotes, (footNoot) => {
            html = html + "** " + footNoot + "</br>";
        });
        return html;
    }

    render() {

        let paginationHtml = '',
            tableData = this._getFinalRenderData();
        if (!this.props.tableData) {
            return (<div className="vymo-table no-more-tables"></div>)
        }

        _.assignIn(this.downloadButton, {onClickParams: _.get(this.props.tableConfigs.downloadConfig, 'reportName', 'download.csv')});

        if (this.state.pagination && !this.state.pagination.dataFromCallBack) {
            paginationHtml = this.state.pagination ? <div className="row">
                    <Pagination totalCount={this.totalCount} onPagination={this._onPagination}
                                numberOfPages={this.totalPages || 0}
                                currentPageNumber={this.searchString ? this.searchStrCount : this.state.currentPage}/>
                </div> : <div></div>;
        } else {
            paginationHtml = this.state.pagination ? <div className="row">
                    <Pagination totalCount={this.totalCount} onPagination={this._onPagination}
                                numberOfPages={this.totalPages || 0}
                                currentPageNumber={this.searchString ? this.searchStrCount : this.state.currentPage}/>
                </div> : <div></div>;
        }
        return (<div className="vymo-table col-md-12">
            <div className="responsive-tables">

                {
                    this.props.tableConfigs.search ?
                        <SearchBox onSearch={this._onSearch}/> : ''
                }

                <TableComponent tableData={tableData} tableConfigs={this.props.tableConfigs}
                                columnConfigs={this.props.columnConfigs} sortOn={this.props.sortOn}
                                onSort={this.props.tableConfigs.defaultSort} sortType={this.props.sortType}
                                currentPage={this.state.currentPage}/>
                {_.get(this.props.tableConfigs, 'footNotes', false) ?
                    <div className="table-foot-notes" dangerouslySetInnerHTML={{__html: this.formFootNotes()}}></div> :
                    <div></div>}
                <div>{_.get(this.props.tableConfigs.downloadConfig, 'isDownloadAble', false) ?
                    <Button button={this.downloadButton}>
                    </Button> : <div></div>}
                </div>
            </div>
            {
                paginationHtml
            }
        </div>)
    }
}

export default TableWrapper;
