/**
 * Created by sameer on 18/05/16.
 */

import React from 'react';
import _ from 'lodash';

class Pagination extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.propTypes = {
            onPagination: React.PropTypes.func.isRequired,
            numberOfPages: React.PropTypes.number.isRequired,
            currentPageNumber: React.PropTypes.number.isRequired
        };
    }

    /**
     * pagination event binder also calls if pagination requires dynamic data loading.
     * @param event
     */
    doPagination(pageNumber, event) {
        let self = this;
        event.preventDefault();
        self.props.onPagination.call(null, pageNumber);
    }


    render() {

        let pages = [];

        pages.push(<li className="total-count pull-left"> <span>Total Count:<span> {this.props.totalCount || 0}</span></span></li>)

        if (this.props.numberOfPages <= 5  && this.props.totalCount > 0) {
            if (this.props.currentPageNumber != 1) {
                pages.push(<li className="paginate-btn"
                               onClick={this.doPagination.bind(this, this.props.currentPageNumber - 1)}>&lang;</li>);
            }
            for (let i = 1; i <= this.props.numberOfPages; i++) {
                pages.push(<li onClick={this.doPagination.bind(this, i)}
                               className={this.props.currentPageNumber == i ? ' paginate-btn active' : 'paginate-btn'}>
                    {i}</li>)
            }

            if (this.props.currentPageNumber != this.props.numberOfPages) {
                pages.push(<li className="paginate-btn"
                               onClick={this.doPagination.bind(this, this.props.currentPageNumber + 1)}>&rang;</li>);
            }


        }else{
            if(this.props.totalCount > 0) {
                 if (this.props.currentPageNumber != 1) {
                    if (this.props.currentPageNumber != 1) {
                        pages.push(<li className="paginate-btn"
                                       onClick={this.doPagination.bind(this, this.props.currentPageNumber - 1)}>&lang;</li>);
                    }
                }
                pages.push(<li onClick={this.doPagination.bind(this, 1)}
                               className={this.props.currentPageNumber == 1 ? ' paginate-btn active' : 'paginate-btn'}>
                    1</li>)

                if (this.props.currentPageNumber > 3 && this.props.currentPageNumber < this.props.numberOfPages - 2) {
                    pages.push(<li className="paginate-btn more">&hellip;</li>);
                    pages.push(<li onClick={this.doPagination.bind(this, this.props.currentPageNumber - 1)}
                                   className=' paginate-btn'>
                        {this.props.currentPageNumber - 1}</li>)
                    pages.push(<li onClick={this.doPagination.bind(this, this.props.currentPageNumber)}
                                   className='active paginate-btn'>
                        {this.props.currentPageNumber}</li>)
                    pages.push(<li onClick={this.doPagination.bind(this, this.props.currentPageNumber + 1)}
                                   className=' paginate-btn'>
                        {this.props.currentPageNumber + 1}</li>)
                    if (this.props.currentPageNumber != this.props.numberOfPages) {
                        pages.push(<li className="paginate-btn more">&hellip;</li>);
                        pages.push(<li onClick={this.doPagination.bind(this, this.props.numberOfPages)}
                                       className=' paginate-btn'>
                            {this.props.numberOfPages}</li>)
                        pages.push(<li className="paginate-btn"
                                       onClick={this.doPagination.bind(this, this.props.currentPageNumber + 1)}>&rang;</li>);
                    }
                } else if (this.props.currentPageNumber <= 3) {
                    for (let i = 2; i <= 5; i++) {
                        pages.push(<li onClick={this.doPagination.bind(this, i)}
                                       className={this.props.currentPageNumber == i ? ' paginate-btn active' : 'paginate-btn'}>
                            {i}</li>)
                    }
                    if (this.props.numberOfPages >= 7) {
                        pages.push(<li className="paginate-btn more">&hellip;</li>);
                        pages.push(<li onClick={this.doPagination.bind(this, this.props.numberOfPages)}
                                       className=' paginate-btn'>
                            {this.props.numberOfPages}</li>)
                    }
                    if (this.props.currentPageNumber != this.props.numberOfPages) {
                        pages.push(<li className="paginate-btn"
                                       onClick={this.doPagination.bind(this, this.props.currentPageNumber + 1)}>&rang;</li>);
                    }

                } else if (this.props.currentPageNumber >= this.props.numberOfPages - 2) {
                    pages.push(<li className="paginate-btn more">&hellip;</li>);
                    for (let i = this.props.numberOfPages - 3; i <= this.props.numberOfPages; i++) {
                        pages.push(<li onClick={this.doPagination.bind(this, i)}
                                       className={this.props.currentPageNumber == i ? ' paginate-btn active' : 'paginate-btn'}>
                            {i}</li>)
                    }
                    if (this.props.currentPageNumber != this.props.numberOfPages) {
                        pages.push(<li className="paginate-btn"
                                       onClick={this.doPagination.bind(this, this.props.currentPageNumber + 1)}>&rang;</li>);
                    }
                }
            }
        }
            let key = 0;
            return (<div className="row">
                <div className="paginate wrapper">
                    <ul>
                        {pages.map(p => {
                            return React.cloneElement(p, {key: key++})
                        })}
                    </ul>
                </div>
            </div>)
        }
    }

export default Pagination;
