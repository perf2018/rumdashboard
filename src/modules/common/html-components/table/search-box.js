/**
 * Created by sameer on 17/05/16.
 */

import React from 'react';
import _ from 'lodash';

class SearchBox extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.propTypes = {
            onSearch: React.PropTypes.func.isRequired
        };
    }

    /**
     * search event binding. search will be handled by table-wrapper function. this only triggers the function.
     * @param event
     */
    doSearch(event) {
        let self = this;
        var newQuery = event.target.value || '';

        self.props.onSearch.call(self, newQuery);
    }

    render() {

        return (
            <div className="row">
                <div className="col-sm-3 col-xs-3 search pull-right">
                    <input type="text" className="pull-right form-control" placeholder="search"
                           onChange={this.doSearch.bind(this)}/></div>
            </div>)
    }
}

function SearchBoxFactory() {
    return SearchBox;
}

export default SearchBox;
