/**
 * Created by sameer on 16/05/16.
 */

function TableDirective(TableWrapper, reactDirective) {
    "use strict";
    return reactDirective(TableWrapper, ['tableData', 'tableConfigs', 'columnConfigs']);
}

TableDirective.$inject = ['TableWrapper', 'reactDirective'];

export default TableDirective;