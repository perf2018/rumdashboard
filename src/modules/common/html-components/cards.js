import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import red from '@material-ui/core/colors/red';
import ChartFactory from '../../../factories/chart-factory'
import MoreVertIcon from '@material-ui/icons/MoreVert';

const styles = theme => ({
  card: {
    marginBottom:"2%",
  },
  cardContent:{
    paddingTop:"0px"
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class GraphViewCard extends React.Component {
  state = { expanded: false };
 
  handleExpandClick = () => {
    this.setState({ expanded: !this.state.expanded });
  };
  
  componentWillMount() {
    console.log("this.props.data",this.props.data);
    this.setState({graph:this.props.data})

  }
  

  render() {
    const { classes } = this.props;
    var cardsDimensions = this.props.cardsDimensions;
    return (
      <div>
        <Card className={classes.card} style={cardsDimensions}>
          <CardHeader
            avatar={
              <Avatar aria-label="Recipe" className={classes.avatar}>
              {this.state.graph.avatar}
              </Avatar>
            }
            action={
              <IconButton>
                <MoreVertIcon />
              </IconButton>
            }
            title={this.state.graph.title}
            subheader={this.state.graph.datadate}
          />
        
          <CardContent className={classes.cardContent}>
            {<ChartFactory className={classes.cardContent} data={this.state.graph}></ChartFactory>}
          </CardContent>

        </Card>
      </div>
    );
  }
}

GraphViewCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GraphViewCard);
