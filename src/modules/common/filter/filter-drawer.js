import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';

import FILTERS from './filter-repo';

import {sideBarItems as menuOptions} from '../../dashboard/mockservice/tileData';
import Filter from './filter';
import _ from "lodash";
import moment from 'moment';
import EventsService from "../utils/events-service";

const styles = theme => ({
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
});

class FilterDrawer extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            open: this.props.filterDrawerOpen,
            filterValues: this.getInitialFilterValues(),
            filterFields: FILTERS,
            setFilters: true
        };

        this.setFilters = this.setFilters.bind(this);
        this.getFilterValues = this.getFilterValues.bind(this);
        this.resetFilters = this.resetFilters.bind(this);
        this.toggleDrawer = this.toggleDrawer.bind(this);
        this.filterOnchangeHandler = this.filterOnchangeHandler.bind(this);
    }

    componentWillMount() {
        EventsService.emitter.on(EventsService.SET_FILTERS, this.setFilters);
        EventsService.emitter.on(EventsService.RESET_FILTERS, this.resetFilters);
        EventsService.emitter.on(EventsService.GET_FILTERS, this.getFilterValues);
    }

    componentWillUnmount() {
        EventsService.emitter.removeListener(EventsService.SET_FILTERS, this.setFilters);
        EventsService.emitter.removeListener(EventsService.RESET_FILTERS, this.resetFilters);
        EventsService.emitter.removeListener(EventsService.GET_FILTERS, this.getFilterValues);
    }

    componentWillReceiveProps(nextProps) {
        this.state.open = nextProps.filterDrawerOpen;
    }


    toggleDrawer() {
        this.setState({
            open: !this.state.open,
        });
    }

    getFilterValues(result) {
        let filterValues = _.cloneDeep(this.state.filterValues);
        filterValues.toDate = filterValues.toDate ? new Date(filterValues.toDate).getTime() : filterValues.toDate;
        filterValues.fromDate = filterValues.fromDate ? new Date(filterValues.fromDate).getTime() : filterValues.fromDate;
        result.filters = {
            toDate: filterValues.toDate,
            fromDate: filterValues.fromDate
        };

    }

    setFilters() {
        this.setState({
            setFilter: true
        })
    }

    resetFilters(cleanFilterField) {
        this.setState({
            filterValues: this.getInitialFilterValues(),
            filters: _.cloneDeep(FILTERS),
            setFilter: !cleanFilterField
        })
    }

    filterOnchangeHandler(id, value) {
        let a= moment(value) > moment().subtract(30, 'd'),b =  moment(value) <= moment().subtract(30, 'm');
        if (id == 'toDate' && moment(value) < moment()) {
            if (moment(value) < moment(this.state.filterValues.fromDate)) {
                this.state.filterValues.fromDate = moment(value).subtract(30, 'm').format('YYYY-MM-DDTHH:mm');
            }
            this.state.filterValues.toDate = moment(value).format('YYYY-MM-DDTHH:mm');
        } else if (id == 'fromDate' && (a || b)) {
            if (moment(value) > moment(this.state.filterValues.toDate)) {
                this.state.filterValues.toDate = moment(value).add(30, 'm').format('YYYY-MM-DDTHH:mm')
            }
            this.state.filterValues.fromDate = moment(value).format('YYYY-MM-DDTHH:mm');
        } else if (id != 'fromDate' && id != 'toDate') {
            this.state.filterValues[id] = value;
        }
        this.setState({});
    }

    getInitialFilterValues() {
        let filterValues = {};
        _.each(_.cloneDeep(FILTERS), f => {
            if (f.id == 'toDate') {
                filterValues[f.id] = moment().format('YYYY-MM-DDTHH:mm');
            }else if (f.id == 'fromDate') {
                filterValues[f.id] = moment().subtract(30, 'd').format('YYYY-MM-DDTHH:mm');
            } else {
                filterValues[f.id] = f.defaultValue || '';
            }
        });

        return filterValues;
    }

    render() {
        const { classes } = this.props;

        return (
            <div>
                <Drawer anchor="right" open={this.state.open} onClose={() => this.props.close()}>
                    <div tabIndex={0} role="button">
                        <Filter filterValues={this.state.filterValues}
                                filterFields={this.state.filterFields}
                                setFilters={this.state.setFilters}
                                resetFilters={this.resetFilters}
                                filterOnchangeHandler={this.filterOnchangeHandler}
                                close={this.props.close}></Filter>
                    </div>
                </Drawer>
            </div>
        );
    }
}

FilterDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FilterDrawer);