import React from 'react';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import _ from 'lodash';
import EventsService from '../utils/events-service';
import ConfigService from '../utils/config-service';

import InputfieldFactory from '../input-fields/input_field_factory';
import Button from '@material-ui/core/Button';
import {withStyles} from "@material-ui/core/styles/index";
import PropTypes from 'prop-types';
import Utils from '../utils/utils';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    gridList: {
        width: 300,
        height: 450,
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
    },
    filters: {
        marginLeft: 10
    }

});

class Filter extends React.Component {
    constructor(props, context) {
        super(props, context);


        this.state = {
            filterValues: this.props.filterValues,
            filterFields: _.cloneDeep(this.props.filterFields),
            setFilters: this.props.setFilters
        };

        this.configService = new ConfigService();
        this.resetFilters = this.resetFilters.bind(this);
        this.filterUpdated = this.filterUpdated.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state.filterValues = nextProps.filterValues;
        this.state.filterFields = nextProps.filterFields;
        this.state.setFilters = nextProps.setFilters;
    }

    resetFilters(cleanFilterField) {
        this.props.resetFilters.call(null, cleanFilterField);
    }

    filterOnchangeHandler(id, value) {
        this.props.filterOnchangeHandler.call(null, id, value);
    }

    filterUpdated() {
        EventsService.emitter.emit(EventsService.FILTERS_UPDATED);
        this.props.close();
    }

    getFilterInputfields() {
        const {classes} = this.props;
        let filterFields = [];
        let keys = Utils.getUniqueKeys(this.state.filterFields.length);
        if (this.state.setFilters) {
            for(let i=0; i<keys.length; i++) {
                let props = _.assignIn(this.state.filterFields[i], {
                    onChange: this.filterOnchangeHandler.bind(this),
                    value: this.state.filterValues[this.state.filterFields[i].id]
                });
                filterFields.push(<GridListTile key={keys[i]} cols={1}>
                    <InputfieldFactory key={keys[i]} config={props}></InputfieldFactory>
                </GridListTile>);
            }
        }

        return filterFields;
    }

    render() {
        const {classes} = this.props;

        return (<div className={classes.filters}>
            <GridList cellHeight={160} className={classes.gridList} cols={1}>
                {this.getFilterInputfields()}
            </GridList>
            <GridList cellHeight={160} className={classes.gridList} cols={2}>
                <GridListTile key={1}>
                    <Button variant="contained" color="secondary" className={classes.button}
                            onClick={() => this.resetFilters}>
                        Reset
                    </Button>
                </GridListTile>
                <GridListTile key={2}>
                    <Button variant="contained" color="primary" className={classes.button}
                            onClick={this.filterUpdated}>
                        Apply
                    </Button>
                </GridListTile>
            </GridList>
        </div>)
    }
}

Filter.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Filter);