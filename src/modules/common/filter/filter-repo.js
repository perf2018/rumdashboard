const VIEW_BY = {
    name: "View By",
    id: "viewBy",
    type: "select",
    defaultValue: "median",
    options: [
        {
            id: "median",
            value: "Median"
        },
        {
            id: "pert_95",
            value: "95th Percentile"
        },
        {
            id: "pert_99",
            value: "95th Percentile"
        }
    ]
}

const DEVICE_TYPE = {
    name: "DEVICE TYPE",
    id: "deviceType",
    type: "select",
    defaultValue: "all",
    options: [
        {
            id: "all",
            value: "ALL DEVICES"
        },
        {
            id: "mobile",
            value: "MOBILE"
        },
        {
            id: "tablet",
            value: "TABLET"
        },
        {
            id: "laptop",
            value: "LAPTOP"
        },
        {
            id: "desktop",
            value: "DESKTOP"
        }
    ]
}


const OS_TYPE = {
    name: "OS TYPE",
    id: "osType",
    type: "select",
    defaultValue: "all",
    options: [
        {
            id: "all",
            value: "ALL OS"
        },
        {
            id: "macos",
            value: "MAC OS"
        },
        {
            id: "windows",
            value: "WINDOWS"
        },
        {
            id: "linux",
            value: "LINUX"
        },
        {
            id: "others",
            value: "OTHERS"
        }
    ]
};

const BROWSER_TYPE = {
    name: "BROWSER TYPE",
    id: "browserType",
    type: "select",
    defaultValue: "all",
    options: [
        {
            id: "all",
            value: "ALL BROWSERS"
        },
        {
            id: "firefox",
            value: "FIREFOX"
        },
        {
            id: "chrome",
            value: "CHROME"
        },
        {
            id: "ie",
            value: "INTERNET EXPLORER"
        },
        {
            id: "safari",
            value: "SAFARI"
        },
        {
            id: "others",
            value: "OTHERS"
        }
    ]
};

const BANDWIDTH_TYPE = {
    name: "BANDWIDTH TYPE",
    id: "bandwidthType",
    type: "select",
    defaultValue: "all",
    options: [
        {
            id: "all",
            value: "ALL"
        },
        {
            id: "less_than_2",
            value: "LESS THAN 2mbps"
        },
        {
            id: "between_2_10",
            value: "BETWEEN 2 and 10mbps"
        },
        {
            id: "more_than_10",
            value: "MORE THAN 10mbps"
        }
    ]
};

const TO_DATE = {
    name: "TO DATE",
    id: "toDate",
    type: "date"
};

const FROM_DATE = {
    name: "FROM DATE",
    id: "fromDate",
    type: "date"
}

var FILTERS = [VIEW_BY, DEVICE_TYPE, OS_TYPE, BROWSER_TYPE, BANDWIDTH_TYPE, TO_DATE, FROM_DATE];
export default FILTERS;

