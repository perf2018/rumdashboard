import $ from "jquery";
import React from 'react';
import ReactDOM from 'react-dom'
import * as d3 from 'd3';
import Rainbow from 'rainbowvis.js';
import {COMMON} from '../utils/common-constants';
import _ from 'lodash';

export class MultiLineChart extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.data = this.props.data.layers;
        this.title = this.props.data.title;
        this.xAxisLabels = this.props.data.xAxisLabels;
        this.draw = this.draw.bind(this);
        this.rainBow = new Rainbow();
        this.state = {
            ignoreLayers: [],
            chartHeight: 0,
            chartWidth: 0
        };
    }

    componentDidMount() {
        var el = ReactDOM.findDOMNode(this);
        this.draw(el);
    }

    componentDidUpdate() {
        var el = ReactDOM.findDOMNode(this);
        this.draw(el);
    }

    draw(el) {

    }

    render() {
        return (
            <div className="chart">
                <div className="chart-title">{this.props.title || this.title}</div>
            </div>
        )
    }
}
