import React, {Component} from 'react';
import {PieChart} from 'react-easy-chart';
import {Legend} from 'react-easy-chart';
import _ from 'lodash';
import ToolTip from './tooltip';

export class PieChartComponent extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            data: this.formatData(this.props.data.data)
        }
    }

    formatData(inputs) {
        return _.map(inputs, input => {
            return {
                key: input.key,
                value: input.count
            }
        })
    }

    mouseOverHandler(d, e) {
        this.setState({
            showToolTip: true,
            top: e.y,
            left: e.x,
            value: d.value,
            key: d.data.key});
    }

    mouseMoveHandler(e) {
        if (this.state.showToolTip) {
            this.setState({top: e.y, left: e.x});
        }
    }

    mouseOutHandler() {
        this.setState({showToolTip: false});
    }

    createTooltip() {
        if (this.state.showToolTip) {
            return (
                <ToolTip
                    top={this.state.top}
                    left={this.state.left}
                >
                    {this.state.key}: {this.state.value}
                </ToolTip>
            );
        }
        return false;
    }

    render() {

        return (
            <div>
                {this.createTooltip()}
                <PieChart
                    labels
                    padding={50}
                    size={600}
                    styles={{
                        '.chart_lines': {
                            strokeWidth: 0
                        },
                        '.chart_text': {
                            fontFamily: 'serif',
                            fontSize: '1.25em',
                            fill: '#333'
                        }
                    }}
                    data={this.state.data}
                    mouseOverHandler={this.mouseOverHandler.bind(this)}
                    mouseOutHandler={this.mouseOutHandler.bind(this)}
                    mouseMoveHandler={this.mouseMoveHandler.bind(this)}
                />
                <Legend data={this.state.data} dataId={'key'} horizontal/>
            </div>
        )
    };

}

export default PieChartComponent;