import React, {Component} from 'react';
import {Chart, Geom, Axis, Tooltip, Coord, Label, Legend, View, Guide, Shape} from 'bizcharts';

import _ from 'lodash';


const Slider = window['bizcharts-plugin-slider'];

const colors = ["l(100) 0:#a50f15 1:#f7f7f7", "l(100) 0:#293c55 1:#f7f7f7"];


let chart;
let data;
export default class Histogram extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            start:this.props.data.data[0],
            end: this.props.data.data[this.props.data.data.length -1],
            data: this.props.data,
            scale: this.getScale(this.props.data.legends)
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state.data = nextProps.data;
        this.state.scale = this.getScale(this.props.data.legends);
    }

    getScale(legends) {
        let scale = {
            time: {
                type: 'time',
                tickCount: 10,
                mask: 'MMM/dd hh:MM'
            }
        };

        _.each(legends, l => {
            scale[l.replace(new RegExp(" ", 'g'), "_")] = {
                alias: l,
                type: 'linear'
            }
        })

        return scale;
    }

    onChange(obj) {
        const {startValue, endValue} = obj;
        this.setState('start', startValue);
        this.setState('end', endValue);
    }

    render() {

        let data = this.state.data;

        return (
            <div>
                <Chart height={400} data={data.data} padding={[40, 40, 40, 80]} scale={this.state.scale}
                       onGetG2Instance={g2Chart => {
                           g2Chart.animate(false);
                           chart = g2Chart;
                       }} forceFit>
                    <Axis name="rain" grid={null}/>
                    <Axis name="flow" title/>
                    <Tooltip/>
                    <Legend custom position="top"
                            items={[
                                {value: data.legends[0], name: data.legends[0], marker: {symbol: 'circle', fill: 'l(100) 0:#a50f15 1:#fee5d9', radius: 5}},
                            {value: data.legends[1], name: data.legends[0], marker: {symbol: 'circle', fill: 'l(100) 0:#293c55 1:#f7f7f7', radius: 5}}
                                ]}
                            onClick={ev => {
                                const item = ev.item;
                                const value = item.value;
                                const checked = ev.checked;
                                const geoms = chart.getAllGeoms();
                                for (let i = 0; i < geoms.length; i++) {
                                    const geom = geoms[i];
                                    if (geom.getYScale().field === value) {
                                        if (checked) {
                                            geom.show();
                                        } else {
                                            geom.hide();
                                        }
                                    }
                                }
                            }}/>
                    {_.map(data.legends, (l, index) => {
                        let position = "time*" + l.replace(new RegExp(" ", 'g'), "_");
                        return <Geom type="area" position={position} color={colors[index]} opacity={0.85}/>
                    })}
                </Chart>


            </div>
        )
    }
}




