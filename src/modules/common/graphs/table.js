import React from 'react';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import _ from "lodash";

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
});

let id = 0;

function createData(name, calories, fat, carbs, protein) {
    id += 1;
    return {id, name, calories, fat, carbs, protein};
}

const rows = [
    createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
    createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
    createData('Eclair', 262, 16.0, 24, 6.0),
    createData('Cupcake', 305, 3.7, 67, 4.3),
    createData('Gingerbread', 356, 16.0, 49, 3.9),
];

class CustomizedTable extends React.Component {

    constructor(props, context) {
        super(props, context);
    }

    render() {
        const {classes} = this.props;
        let data = this.props.data.data;
        let rows = _.map(data.tableData, (d, index) => {
            return {
                id: index,
                ...d
            }
        })
        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            {_.map(data.headers, h => (
                                <CustomTableCell>{h.name}</CustomTableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {_.map(rows, row => {
                            return (
                                <TableRow className={classes.row} key={row.id}>
                                    {_.map(data.headers, h => {
                                        if(h.id == 'experience') {
                                            return <CustomTableCell component="th" scope="row">
                                                {_.map(row[h.id], r => {
                                                    return <div>
                                                        <span>{r.key + " - "}</span>
                                                        <span>{r.traffic}</span>
                                                    </div>
                                                })}
                                            </CustomTableCell>
                                        } else {
                                            return <CustomTableCell component="th" scope="row">
                                                {row[h.id]}
                                            </CustomTableCell>
                                        }

                                    })}
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </Paper>
        );
    }

}


export default withStyles(styles)(CustomizedTable);