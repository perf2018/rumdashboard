/**
 * Created by sameer on 13/10/16.
 */

import React from 'react';
import _ from 'lodash';
import eventsService from '../../../services/events-service';
import ReactTooltip from "react-tooltip";
import StingUtils from '../../../utils/string-utils'



class CheckBoxGroup extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            values: this.props.values || [],
            errorVisible: false,
            errorMessage: '',
            id: this.props.id
        }
        this._validate = this._validate.bind(this);
    }

    componentWillMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this._validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this._validate);
        }
    }


    _getCheckboxes() {

        return _.map(this.props.options, o => {
            let props = this._prepareEachProp(o)
            return (<div className="checkbox">
                <label>
                <input {...props}/>
                <span>{props.label}</span></label>
                </div>)
        })
    }

    _prepareEachProp(option) {
        let props = {
            label: option.label,
            type: 'checkbox',
            onChange: this._handleChange.bind(this),
            id: _.camelCase(option.label),
            value: option.value,
            checked: this.state.values.indexOf(option.value) > -1 ? true : false,
            required: this.props.required
        }

        return props;
    }

    _handleChange(e) {
        let values = this.state.values
        if(e.target.checked) {
            values.push(e.target.value);
        } else {
            _.remove(values, v => {
                return v == e.target.value;
            })
        }

        this.setState({
            values: values
        })
    }

    _validate(result) {
        let valid=true;

        if (this.props.required && !this.state.values.length) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: StringUtils.getLabel("ERROR_MESSAGE"),
                valid: false
            });
        }

        if(valid) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }

        result.valid &= valid;
        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: this.state.values,
            name: this.props.label
        });
    }

    render() {
        return (<div className={this.props.fieldParentClass}>
                <label for={this.props.id}><span>{this.props.label}</span>
                    <span>{this.props.required ? '*' : ''}</span>
                    <i className={this.props.helperText ? "fa fa-info-circle info-bubble" : "hide"} aria-hidden="true" data-tip={this.props.helperText}></i>
                </label>
                {this._getCheckboxes()}
                <div className='errorField'>{this.state.errorMessage}</div>
            <ReactTooltip/>
        </div>);
    }
}

export default CheckBoxGroup;
