import React from 'react';
import NativeSelect from '@material-ui/core/NativeSelect';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 220,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class SelectField extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.config.id,
            value: this.props.config.value
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.config.id,
            value: nextProps.config.value
        }
    }

    handleChange(event) {
        //this.setState({ value: event.target.value });
        this.props.config.handlechange.call(null, this.state.id, event.target.value);
    };

    render() {
        const {classes} = this.props;
        return (<FormControl className={classes.formControl} disabled={this.props.disabled}>
            <InputLabel htmlFor={this.props.config.id}>{this.props.config.name}</InputLabel>
            <NativeSelect key={this.props.config.key} onChange={this.handleChange.bind(this)} {...this.props.config}>
                {this.props.config.options}
            </NativeSelect>
            <FormHelperText>{this.props.helperText}</FormHelperText>
        </FormControl>)
    }
}

export default withStyles(styles)(SelectField);