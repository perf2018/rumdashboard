/**
 * Created by sameer on 02/06/16.
 */

import React from 'react';
import {DropdownList} from 'react-widgets';
import _ from 'lodash';
import eventsService from '../../../services/events-service';
import ReactTooltip from "react-tooltip";
import InputField from '../form-fields/input-field-generation';
import StringUtils from '../../../utils/string-utils';

class DropDownAutoComplete extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.id,
            value: this.props.value ? this._getValue(this.props.value) : '',
            valid: true,
            errorVisible: false,
            errorMessage: '',
            filter: this.props.filterType,
            idField: this.props.valueField,
            textField: this.props.textField,
            oifFields: []
        };
        this._validate = this._validate.bind(this);
    }

    _getValue(value) {
        const {returnCode, options} = this.props;
        if (typeof value == 'string') {
            value = _.unescape(value);
            if (!_.isUndefined(returnCode) && !returnCode) {
                if (!_.isEmpty(options)) {
                    var option = _.find(options, {code: value});
                    return _.get(option, 'name', value);
                } else {
                    return value
                }
            } else {
                return value
            }
        } else if (_.isObject(value)) {
            if (_.isObject(value.code)) {
                return value;
            } else {
                if (!_.isUndefined(returnCode) && returnCode) {
                    return value.code;
                } else {
                    return value.name;
                }
            }
        } else {
            return value;
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            value: nextProps.value ? this._getValue(nextProps.value) : '',
            valid: true,
            errorVisible: false,
            errorMessage: '',
            filter: nextProps.filterType,
            idField: nextProps.valueField,
            textField: nextProps.textField
        };
    }

    componentDidMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this._validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this._validate);
        }
    }

    getOifFields() {
        const self = this;
        if (this.state.oifFields.inputs.length > 0) {
            return this.state.oifFields.inputs.map((config, index) => {
                _.assignIn(config, {
                    fieldParentClass: 'form-group col-md-6',
                    eventName: self.props.eventName,
                    HttpService: self.props.HttpService,
                    CommonUtilsService: self.props.CommonUtilsService
                });
                return <InputField key={index} config={config}/>;
            });
        } else {
            return null;
        }
    }

    changeValue(value) {
        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, value);
        }

        if (this.props.type != 'code_name_spinner' && value === 'Select') {
            return;
        }

        var oifOption = _.get(this.props, 'oif_options');
        if (!_.isEmpty(oifOption)) {
            var oifUrl = _.get(this.props, 'oif_options.url');
            if (!oifUrl) {
                return;
            }
            oifUrl = oifUrl.replace(/\?$/, '');

            var oifParams = _.get(this.props, 'oif_options.params', []);

            if (!oifParams.length) {
                return;
            } else {
                var params = {};
                _.each(oifParams, param => {
                    if (value) {
                        params[param.code] = this.props.type === 'code_name_spinner' ? value.code : value;
                    }
                });
                this.props.CommonUtilsService.showLoader();
                const self = this;
                this.props.HttpService._post(oifUrl, {}, params)
                    .then((result) => {
                        self.setState({
                            oifFields: result
                        });
                        self.props.CommonUtilsService.hideLoader();
                    })
                    .catch((error) => {
                        self.setState({errorMessage: error});
                    })
                    .finally(() => {
                        self.props.CommonUtilsService.hideLoader();
                    });
            }
        }

        if (value) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }


        this.setState({
            value: value,
            valid: true
        });
    }

    _validate(result) {
        let valid = true;
        const defaultValue = _.get(this.state, 'value', 'NOT_VALID_VALUE');
        if (this.props.required && (defaultValue == 'NOT_VALID_VALUE' || defaultValue === '')) {
            this.setState({
                errorVisible: true,
                errorMessage: StringUtils.getLabel("ERROR_MESSAGE"),
                valid: false
            });
            valid = false;
        }


        if (valid) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }

        if (this.props.type === 'multi_select_auto_complete' && this.props.singleSelect) {
            if (this.state.value && typeof this.state.value === 'object') {
                let valArray = [];
                valArray.push(this.state.value);
                this.state.value = JSON.stringify(valArray);
            }
        }
        const { value, textField } = this.state;
        const { returnCode } = this.props;
        result.valid &= valid;
        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: _.isObject(value) ? (returnCode ? (value.code) : (value.name || value[textField])) : value,
            name: this.props.label
        })
    }

    getDropDownList() {
        return (
            <div className={this.props.fieldParentClass}>
                <label forHtml={this.props.label}><span>{this.props.label}</span>
                    <span>{this.props.required ? '*' : ''}</span>
                    <i className={this.props.helperText ? "fa fa-info-circle info-bubble" : "hide"} aria-hidden="true"
                       data-tip={this.props.helperText}></i>
                </label>
                <DropdownList
                    data={this.props.options}
                    onChange={this.changeValue.bind(this)}
                    valueField={this.state.idField} textField={this.state.textField}
                    caseSensitive={false}
                    filter={this.state.filter}
                    defaultValue={this.state.value}
                    disabled={this.props.disabled}
                    HttpService={this.props.HttpService}
                    CommonUtilsService={this.props.CommonUtilsService}
                >
                </DropdownList>
                <div className='errorField'>{this.state.errorMessage}</div>
                <ReactTooltip/>
            </div>
        )
    }

    getOIFWithDropDown() {
        return (
            <div>
                {this.getDropDownList()}
                {!_.isEmpty(this.state.oifFields) ? this.getOifFields() : null}
            </div>
        )
    }

    render() {
        if (!_.isEmpty(this.props.options) && this.props.options.length > 10) {
            this.state.filter = 'contains';
        }

        if (!_.isEmpty(this.state.oifFields)) {
            return this.getOIFWithDropDown();
        } else {
            return this.getDropDownList();
        }
    }

}

export default DropDownAutoComplete;
