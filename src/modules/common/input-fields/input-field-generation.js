/**
 * Created by sameer on 14/06/16.
 */

'user strict'

import React from 'react';
import _ from 'lodash';
import InputField from './input-component';
import DropDown from './dropdown-autocomplete-component';
import DropdownMultiSelect from './dropdown-multiselect';
import DateInput from './date-input';
import {EventEmitter} from 'events';
import CheckBox from  './checkbox-component';
import CheckBoxGroup from './checkbox-group';
import TagInput from './tag-input';

class InputFieldGeneration extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        let inputProps = {};
        switch (this.props.config.type) {
            case  "code_name_spinner":
                inputProps = {
                    id: this.props.config.code,
                    name: this.props.config.hint,
                    label: this.props.config.hint,
                    value:  this.props.config.value,
                    options: this.props.config.options,
                    valueField: this.props.config.valueField || 'code',
                    textField: this.props.config.textField || 'name',
                    filterType: this.props.config.filterType,
                    required: this.props.config.required,
                    onChange: this.props.config.onChange,
                    disabled: this.props.config.disabled,
                    fieldParentClass: this.props.config.fieldParentClass,
                    eventName: this.props.config.eventName,
                    type: this.props.config.type,
                };

                return <DropDown {...inputProps}></DropDown>
            case  "spinner":
                inputProps = {
                    id: this.props.config.code,
                    name: this.props.config.hint,
                    label: this.props.config.hint,
                    value: this.props.config.value,
                    options: this.props.config.spinner_options,
                    required: this.props.config.required,
                    onChange: this.props.config.onChange,
                    disabled: this.props.config.disabled,
                    defaultValue: this.props.config.value,
                    fieldParentClass: this.props.config.fieldParentClass,
                    eventName: this.props.config.eventName,
                    type: this.props.config.type,
                    helperText: this.props.config.helperText,
                };
                return <DropDown {...inputProps}></DropDown>
            case "multi_select_check_box":
                inputProps = {
                    id: this.props.config.code,
                    name: this.props.config.hint,
                    label: this.props.config.hint,
                    data: _.map(this.props.config.options, o => {
                        return {
                            value: o.code,
                            label: o.name,
                            selected: _.findIndex(value, {value: o.code}) != -1
                        }
                    }),
                    valueField: this.props.config.valueField,
                    textField: this.props.config.textField,
                    filterType: this.props.config.filterType,
                    required: this.props.config.required,
                    onChange: this.props.config.onChange,
                    disabled: this.props.config.readonly || this.props.config.disabled,
                    fieldParentClass: this.props.config.fieldParentClass,
                    eventName: this.props.config.eventName,
                    type: this.props.config.type,
                    value: this.props.config.value

                };

                return <DropdownMultiSelect {...inputProps}></DropdownMultiSelect>

            case "date":
            case "datetime":
                inputProps = {
                    value: this.props.config.value,
                    type: this.props.config.type,
                    id: this.props.config.code,
                    disabled: this.props.config.disabled || this.props.config.readonly ? "true" : null,
                    required: this.props.config.required,
                    label: this.props.config.hint,
                    fieldParentClass: this.props.config.fieldParentClass,
                    data: this.props.config.data,
                    onChange: this.props.config.onChange,
                    eventName: this.props.config.eventName,
                    min: this.props.config.min || this.props.config.min_date,
                    max: this.props.config.max || this.props.config.max_date,
                    meetingParentClass: this.props.config.meetingParentClass,
                    durationClass: this.props.config.durationClass || this.props.config.fieldParentClass,
                };
                return <DateInput {...inputProps}></DateInput>

            case "checkbox":
                inputProps = {
                    value: this.props.config.value,
                    type: this.props.config.type,
                    id: this.props.config.code,
                    disabled: this.props.config.disabled ? true : null,
                    label: this.props.config.hint,
                    onChange: this.props.config.onChange,
                    fieldParentClass: this.props.config.fieldParentClass,
                    eventName: this.props.config.eventName,
                    required: this.props.config.required,
                    customLabel: this.props.config.customLabel
                };

                return <CheckBox {...inputProps}></CheckBox>

            case "checkbox_group":
                inputProps = {
                    options: this.props.config.options,
                    type: this.props.config.type,
                    id: this.props.config.code,
                    label: this.props.config.hint,
                    onChange: this.props.config.onChange,
                    fieldParentClass: this.props.config.fieldParentClass,
                    eventName: this.props.config.eventName,
                    helperText: this.props.config.helperText,
                    values: this.props.config.values,
                    required: this.props.config.required
                };

                return <CheckBoxGroup {...inputProps}></CheckBoxGroup>

            case "tag_input":
                inputProps = {
                    value: this.props.config.value,
                    type: this.props.config.type,
                    id: this.props.config.code,
                    label: this.props.config.hint,
                    onChange: this.props.config.onChange,
                    fieldParentClass: this.props.config.fieldParentClass,
                    eventName: this.props.config.eventName,
                    required: this.props.config.required,
                    placeholder: this.props.config.placeholder
                };

                return <TagInput {...inputProps}></TagInput>

            default:
                inputProps = {
                    value: this.props.config.value,
                    type: this.props.config.type,
                    placeholder: this.props.config.placeholder || '',
                    id: this.props.config.code,
                    maxlength: this.props.config.maxLength,
                    disabled: this.props.config.disabled ? "true" : null,
                    min: this.props.config.min || this.props.config.min_chars,
                    max: this.props.config.max || this.props.config.max_chars,
                    required: this.props.config.required,
                    label: this.props.config.hint,
                    onChange: this.props.config.onChange,
                    fieldParentClass: this.props.config.fieldParentClass,
                    readonly: this.props.config.readonly,
                    eventName: this.props.config.eventName,
                };
                return <InputField {...inputProps}></InputField>
        }
    }
}

export default InputFieldGeneration;
