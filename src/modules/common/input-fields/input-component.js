/**
 * Created by sameer on 09/06/16.
 */

import React from 'react';
import Types from './types';
import eventsService from '../utils/events-service';

class InputField extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.id,
            value: this._getInputFieldValue(this.props) || '',
            valid: true,
            errorVisible: false,
            errorMessage: ''
        };

        this.validate = this.validate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            value: this._getInputFieldValue(nextProps) || '',
            valid: true,
            errorVisible: false,
            errorMessage: ''
        };
    }

    componentWillMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this.validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this.validate);
        }
    }

    _handleChange(event) {

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, event.target.value);
        }
        this.setState({
            value: event.target.value
        });
    }

    _getClasses(classes) {
        if (classes) {
            return classes.join(' ');
        }
    }

    _getInputFieldProps() {
        let inputProps = {
            value: stringUtils.htmlDecode(this.state.value),
            type: this.props.type,
            placeholder: this.props.placeholder || '',
            id: this.props.id,
            onChange: this._handleChange.bind(this),
            className: this._getClasses(this.props.classes) ? this._getClasses(this.props.classes) + 'form-control' : 'form-control',
            maxlength: this.props.maxLength,
            disabled: this.props.disabled ? "true" : null,
            min: this.props.min,
            max: this.props.max,
            readOnly: this.props.readonly ? "true" : null,
            required: this.props.required,
            helperText: this.props.helperText,
            onBlur: this.onBlurValidate.bind(this)
        };
        return inputProps;
    }

    onBlurValidate() {
        this._validate();
    }

    validate(result) {
        result.valid &= this._validate();

        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: this.state.value,
            name: this.props.label
        });
    }

    _validate() {
        let regEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            regNum = /^\d*$/,
            regPhoneNum = /^(\+?([0-9]{2})\)?[-. ]?([0-9]{10}))|([0-9]{10})$/,
            regDecimal = /^\d+(\.\d{1,2})?$/,
            valid = true;

        if (this.props.required && !this.state.value) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: 'This is required field.',
                valid: false
            });
        }

        if (this.state.value) {
            // check for min
            if (this.props.min && (this.props.min > this.state.value.length)) {
                valid = false;
            }
            // check for max
            if(this.props.max && (this.props.max < this.state.value.length)){
                vaild = false
            }

            if (!valid) {
                this.setState({
                    errorVisible: true,
                    errorMessage: 'Length of field is not proper.',
                    valid: false
                });
            }
        }


        if ((this.props.required && this.props.type === 'email' && !regEmail.test(this.state.value))
            || (this.state.value && this.props.type === 'email' && !regEmail.test(this.state.value))) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: 'This is not a valid email id.',
                valid: false
            });
        }

        if (this.state.value && this.props.type === 'number' && !regNum.test(this.state.value)) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: 'This is not a valid number.',
                valid: false
            });
        }

        if (this.state.value && this.props.type === 'phone' && !regPhoneNum.test(this.state.value)) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: 'This is not a valid phone number.',
                valid: false
            });
        }

        if (this.state.value && this.props.type === 'decimal' && !regDecimal.test(this.state.value)) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: 'This is not a valid decimal number.',
                valid: false
            });
        }

        if (valid) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }

        return valid;
    }

    _getInputFieldValue(props) {
        switch (props.type) {
            case Types.NUMBER:
            case Types.EMAIL:
            case Types.DECIMAL:
            case Types.PHONE:
                if (!this.props.isMasked) {
                    return props.value;
                }
                var value = props.value;
                const maskLength = this.props.maskLength;
                if (!value || value.length <= maskLength) {
                    return 'xxxxxxxxxx';
                }
                var maskedString = '';
                for (var i = 0; i < value.length; i++) {
                    if ((value.length-i) <= maskLength) {
                        maskedString += 'x';
                    } else {
                        maskedString += value[i];
                    }
                }
                return maskedString;
            case Types.TEXT:
            case Types.PASSWORD:
                return props.value;
        }
    }

    render() {
        let props = this._getInputFieldProps();
        return (<div className={this.props.fieldParentClass}>
            <label for={this.props.id}><span>{this.props.label}</span>
                <span className="required">{props.required ? '*' : ''}</span>
                <i className={props.helperText ? "fa fa-info-circle info-bubble" : "hide"} aria-hidden="true"
                   data-tip={props.helperText}></i>
            </label>
            <input autoComplete={'off'} {...props}/>
            <div className='errorField'>{this.state.errorMessage}</div>
            <ReactTooltip/>
        </div>)
    }

}

export default InputField;
