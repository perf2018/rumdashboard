/**
 * Created by sameer on 15/06/16.
 */

import React from 'react';
import moment from 'moment';
import Types from './types';
import _ from 'lodash';
import {DateTimePicker} from 'react-widgets';
import momentLocalizer from 'react-widgets/lib/localizers/moment';
import InputField from '../../html-components/form-fields/input-field-generation';
import eventsService from '../../../services/events-service';
import stringUtils from '../../../utils/string-utils';

momentLocalizer(moment);

class DateInput extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            id: this.props.id,
            type: this.props.type,
            valid: true,
            errorMessage: '',
            errorVisible: false

        };
        this._setInputFieldValue(this.props);
        this._validate = this._validate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            type: nextProps.type,
            valid: true,
            errorVisible: false,
            errorMessage: ''
        };
        this._setInputFieldValue(nextProps);
    }

    componentWillMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this._validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this._validate);
        }
    }

    _handleChange(date) {
        let value;
        if (this.state.type == 'meeting') {
            value = {
                date: new Date(date),
                duration: this.state.value.duration
            }
        } else {
            value = date
        }

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, value);
        }
        this.setState({
            value: value,
            valid: true,
            errorVisible: false,
            errorMessage: '',
        });
    }

    _handleDurationChange(id, value) {

        let formatedValue;

        formatedValue = {
            date: this.state.value.date,
            duration: parseInt(stringUtils.getStanitizedString(value.code))
        };

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, formatedValue);
        }
        this.setState({
            value: formatedValue
        })
    }

    _getClasses(classes) {
        if (classes) {
            return classes.join(' ');
        }
    }


    _setInputFieldValue(props) {
        let date;
        switch (props.type) {
            case Types.DATE:
                date = props.data && props.data.timestamp ? new Date(props.data.timestamp) : (props.value ? new Date(props.value) : null);
                _.assignIn(this.state, {
                    value: date
                });
                break;
            case Types.TIME:
                _.assignIn(this.state, {value: date.format('h:mm A')});
                break;
            case Types.DATETIME:
                _.assignIn(this.state, {value: date.format('MMM D h:mm A')});
                break;
            case Types.MEETING:
                date = props.data && props.data.date ? new Date(props.data.date) : null;
                _.assignIn(this.state, {
                    value: {
                        date: date,
                        duration: props.data && props.data.duration ? props.data.duration / 60000 : 0
                    }
                });
                break;
        }
    }

    _getDurationHtml() {
        let propsDuration = {
            value: this.state.value.duration,
            type: 'code_name_spinner',
            code: 'duration',
            label: 'Duration',
            hint: 'Duration(in min)',
            onChange: this._handleDurationChange.bind(this),
            className: 'form-control',
            fieldParentClass: this.props.durationClass,
            disabled: this.props.disabled ? true : null,
            readOnly: this.props.readonly ? true : null,
            required: this.props.required,
            code_name_spinner_options: this.props.code_name_spinner_options || this.props.VymoDataService.getDurationOptions()
        };

        return <InputField config={propsDuration}></InputField>
    }

    _validate(result) {
        let valid = true;
        if (this.props.required && ((this.props.type == 'meeting' && !this.state.value.date) || (this.props.type !=  'meeting' && !this.state.value))) {
            this.setState({
                errorVisible: true,
                errorMessage: 'This is required field.',
                valid: false
            });
            valid = false;
        }

        if (valid) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }

        let value;
        if (this.state.type == 'meeting') {
            value = {
                date: moment(this.state.value.date).valueOf(),
                duration: this.state.value.duration
            }
        } else {
            value = this.state.value ? moment(this.state.value).valueOf() : null
        }

        result.valid &= valid;
        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: value,
            name: this.props.label
        })
    }

    _getDateField() {
        const isMeeting = _.isEqual(this.props.type, 'meeting');
        let meetingMinDate = moment().add(this.props.min, 'milliseconds');
        const now = moment();
        if (isMeeting) {
            const minute = now.minute();
            if (minute < 30) {
                meetingMinDate.minute(30)
            }
            else {
                meetingMinDate.minute(0)
            }
        }
        let minDate = (this.props.min || isMeeting) ? meetingMinDate.toDate() : null;
        let defaultValue = this.props.type == 'meeting' ? this.state.value.date : this.state.value;
        // defaultValue should be a `Date` instance or `null`
        if (!moment(defaultValue).isValid() || _.isUndefined(defaultValue)) {
            defaultValue = null;
        }
        return (<div className={this.props.fieldParentClass}>
            <label
                htmlFor={this.props.id}><span>{this.props.label}</span>
                <span>{this.props.required ? '*' : ''}</span></label>
            <DateTimePicker defaultValue={defaultValue}
                            onChange={this._handleChange.bind(this)}
                            disabled={this.props.disabled} time={this.props.type == 'meeting'}
                            step={this.props.type == 'meeting' ? 30 : null}
                            format={this.props.type == 'meeting' ? "MMM DD YYYY h:mm A" : "MMM DD YYYY"}
                            min={minDate}></DateTimePicker>
             <div className='errorField'>{this.state.errorMessage}</div>
        </div>)
    }

    render() {
        let fields = [];
        fields.push(this._getDateField());

        if (this.props.type == 'meeting') {
            fields.push(this._getDurationHtml());
        }
        return (<div className={this.props.meetingParentClass}>{fields}</div>)
    }
}

export default DateInput;
