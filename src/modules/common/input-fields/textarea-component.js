/**
 * Created by sameer on 22/11/16.
 */
'use strict'

import React from 'react';
import eventsService from '../../../services/events-service';
import stringUtils from '../../../utils/string-utils';
import ReactTooltip from "react-tooltip";

class TextArea extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.id,
            value: this.props.value || '',
            valid: true,
            errorVisible: false,
            errorMessage: ''
        };

        this.validate = this.validate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            value: nextProps.value || '',
            valid: true,
            errorVisible: false,
            errorMessage: ''
        };
    }

    componentWillMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this.validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this.validate);
        }
    }

    _handleChange(event) {

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, event.target.value);
        }
        this.setState({
            value: stringUtils.getStanitizedString(event.target.value)
        });
    }

    _getClasses(classes) {
        if (classes) {
            return classes.join(' ');
        }
    }

    _getInputFieldProps() {
        let inputProps = {
            value: this.state.value,
            type: this.props.type,
            placeholder: this.props.placeholder || '',
            id: this.props.id,
            onChange: this._handleChange.bind(this),
            className: this._getClasses(this.props.classes) ? this._getClasses(this.props.classes) + 'form-control' : 'form-control',
            maxlength: this.props.maxLength,
            disabled: this.props.disabled ? "true" : null,
            min: this.props.min,
            max: this.props.max,
            readOnly: this.props.readonly ? "true" : null,
            required: this.props.required,
            helperText: this.props.helperText,
            onBlur: this.onBlurValidate.bind(this),
            rows: this.props.rows,
            cols: this.props.cols
        };

        return inputProps;
    }

    onBlurValidate() {
        this._validate();
    }

    validate(result) {
        result.valid &= this._validate();

        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: this.state.value,
            name: this.props.label
        });
    }

    _validate() {

        if (this.props.required && !this.state.value) {
            this.setState({
                errorVisible: true,
                errorMessage: StringUtils.getLabel("ERROR_MESSAGE"),
                valid: false
            });
            return false;
        } else {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
            return true;
        }
    }

    render() {
        let props = this._getInputFieldProps();
        return (<div className={this.props.fieldParentClass}>
            <label htmlFor={this.props.id}><span>{this.props.label}</span>
                {props.required ? <span className="required">*</span> : <span></span>}
                <i className={props.helperText ? "fa fa-info-circle info-bubble" : "hide"} aria-hidden="true"
                   data-tip={props.helperText}></i>
            </label>
            <textarea autoComplete={'off'} {...props}></textarea>
            <div className='errorField'>{this.state.errorMessage}</div>
            <ReactTooltip/>
        </div>)
    }

}

export default TextArea;
