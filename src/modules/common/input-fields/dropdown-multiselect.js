/**
 * Created by sameer on 15/06/16.
 */

import React from 'react';
import _ from 'lodash';
import Multiselect from 'react-bootstrap-multiselect';
import eventsService from '../../../services/events-service';
import StringUtils from '../../../utils/string-utils';

class DropdownMultiSelect extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.id,
            data: this.props.options,
            selected: [],
            valid: true,
            errorVisible: false,
            errorMessage: '',
            value: _.clone(this.props.value)
        }

        this._validate = this._validate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            data: nextProps.options,
            selected: [],
            valid: true,
            errorVisible: false,
            errorMessage: '',
            value: _.clone(this.props.value)
        };
    }

    componentWillMount() {
        if(this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this._validate);
        }
    }

    componentWillUnmount() {
        if(this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this._validate);
        }
    }

    _validate(result) {

        let valid = true;
        if (this.props.required && !this.state.value) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: StringUtils.getLabel("ERROR_MESSAGE"),
                valid: false
            });
        }


        if(valid) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }

        result.valid &= valid;
        var values = [];
        _.each(this.state.value, (value) => {
            var valueObj = _.find(this.props.data, {value: value});
            values.push({
                code: valueObj.value,
                name: valueObj.label
            })
        })
        values = JSON.stringify(values);

        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: _.isArray(this.state.value) ? values : this.state.value ,
            name: this.props.label
        })
    }

    _handleChange() {

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, event.target.value, event.target.checked);
        }else{
            this.state.value= this.state.value||[];
            if(event.target.checked){
                this.state.value.push(event.target.value);
            }else {
                _.pull(this.state.value,event.target.value );
            }
        }

        this.setState({
            selected: event.target.value,
            valid: true
        });
    }

    render() {
        return <div className={this.props.fieldParentClass}>
            <label htmlFor={this.props.label}><span>{this.props.label}</span><span>{this.props.required ? '*' : ''}</span></label>
            <Multiselect maxHeight={200}
                         data={this.props.data}
                         multiple
                         disabled={this.props.disabled}
                         onChange={this._handleChange.bind(this)}
                         includeSelectAllOption = {!this.props.singleSelect && this.props.includeSelectAllOption }
                         enableFiltering = {this.props.enableFiltering}
                         enableCaseInsensitiveFiltering = {this.props.enableCaseInsensitiveFiltering}>

            </Multiselect>
            <div className='errorField'>{this.state.errorMessage}</div>
        </div>
    }
}

export default DropdownMultiSelect;
