/**
 * Created by sameer on 08/08/16.
 */

import React from 'react';
import eventsService from '../../../services/events-service';
import StringUtils from '../../../utils/string-utils';

class CheckboxComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            id: this.props.id,
            value: this.props.value,
        }

        this._validate = this._validate.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            value: nextProps.value || false,
        };
    }

    componentWillMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this._validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this._validate);
        }
    }

    _handleChange(event) {

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, event.target.value);
        }
        this.setState({
            value: event.target.checked
        });
    }

    _getClasses(classes) {
        if (classes) {
            return classes.join(' ');
        }
    }

    _getInputFieldProps() {
        let inputProps = {
            value: this.state.value,
            type: this.props.type,
            id: this.props.id,
            onChange: this._handleChange.bind(this),
            className: this._getClasses(this.props.classes) ? this._getClasses(this.props.classes) + 'form-control' : 'form-control',
            disabled: this.props.disabled ? "true" : null,
            readOnly: this.props.readonly ? "true" : null,
            checked: this.state.value,
            required: this.props.required,
            customLabel: this.props.customLabel
        };

        return inputProps;
    }

    _validate(result) {

        let valid=true;

        if (this.props.required && !this.state.value) {
            valid = false;
            this.setState({
                errorVisible: true,
                errorMessage: StringUtils.getLabel("ERROR_MESSAGE"),
                valid: false
            });
        }

        if(valid) {
            this.setState({
                errorVisible: false,
                errorMessage: '',
                valid: true
            });
        }

        result.valid &= true;
        result.values.push({
            type: this.props.type,
            code: this.state.id,
            value: this.state.value,
            name: this.props.label
        });
    }


    render() {
        let props = this._getInputFieldProps();
        return (<div className={this.props.fieldParentClass}>
            <label for={this.props.id}>
                <input {...props}/>
                {props.customLabel ? <span dangerouslySetInnerHTML={{__html: props.customLabel}}></span> : <span>{this.props.label}</span>}
            </label>
            <div className='errorField'>{this.state.errorMessage}</div>
        </div>)
    }
}

export default CheckboxComponent;
