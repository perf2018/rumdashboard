/**
 * Created by pranita on 03/01/17.
 */
import React from 'react';

class LinkComponent extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div className="form-group col-lg-6 location">
                <a href={this.props.url} target="_blank"> {this.props.title} </a>
            </div>
        )
    }

}
export default LinkComponent;
