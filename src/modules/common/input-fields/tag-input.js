/**
 * Created by sameer on 25/10/16.
 */

import React from 'react';
import {WithContext as ReactTags} from 'react-tag-input';

class TagInput extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.id,
            tags: this.props.value || [],
            valid: true,
            errorVisible: false,
            errorMessage: ''
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.id,
            tags: nextProps.value,
            valid: true,
            errorVisible: false,
            errorMessage: ''
        };
    }

    componentWillMount() {
        if (this.props.eventName) {
            this._subscription = eventsService.emitter.addListener(this.props.eventName, this._validate);
        }
    }

    componentWillUnmount() {
        if (this.props.eventName) {
            eventsService.emitter.removeListener(this.props.eventName, this._validate);
        }
    }

    handleDelete(i) {
        let tags = this.state.tags;
        tags.splice(i, 1);

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, tags);
        }
        this.setState({tags: tags});
    }

    handleAddition(tag) {
        let tags = this.state.tags;
        tags.push({
            id: _.camelCase(tag),
            text: tag
        });

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, tags);
        }
        this.setState({tags: tags});
    }

    handleDrag(tag, currPos, newPos) {
        let tags = this.state.tags;

        // mutate array
        tags.splice(currPos, 1);
        tags.splice(newPos, 0, tag);

        if (this.props.onChange) {
            this.props.onChange.call(null, this.state.id, tags);
        }
        // re-render
        this.setState({tags: tags});
    }

    render() {
        return (<div className="row">
            <div className="col-md-12">
                <ReactTags tags={this.state.tags}
                           handleDelete={this.handleDelete.bind(this)}
                           handleAddition={this.handleAddition.bind(this)}
                           handleDrag={this.handleDrag.bind(this)}
                           placeholder={this.props.placeholder}></ReactTags>
            </div>
        </div>)
    }
}

export default TagInput;
