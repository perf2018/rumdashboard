import React from 'react';
import _ from 'lodash';
import SelectField from './select-field';
import DateField from './date-field';

export default class InputFieldFactory extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {

        let props = {};
        switch (this.props.config.type) {
            case 'select':
                props = {
                    name: this.props.config.name,
                    id: this.props.config.id,
                    value: this.props.config.value,
                    handlechange: this.props.config.onChange,
                    InputLabelProps: {},
                    key: this.props.config.key,
                    options: _.map(this.props.config.options, o => {
                        return <option value={o.id}>{o.value}</option>
                    })
                };

                return <SelectField native config={props} disabled={this.props.config.disabled}
                                    helperText={this.props.config.helperText}/>


            case 'date':
                props = {
                    id: this.props.config.id,
                    label: this.props.config.name,
                    defaultValue: this.props.config.value,
                    handlechange: this.props.config.onChange,
                    key: this.props.config.key,
                    type: 'datetime-local',
                    InputLabelProps: {
                        shrink: true
                    }
                }
                return <DateField config={props} disabled={this.props.config.disabled}
                                  helperText={this.props.config.helperText}/>

        }
    }
}