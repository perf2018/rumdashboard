import React from 'react';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import {withStyles} from '@material-ui/core/styles';

const styles = theme => ({
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        width: 200
    }
});

class DateField extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            id: this.props.config.id,
            value: this.props.config.defaultValue
        }
    }

    componentWillReceiveProps(nextProps) {
        this.state = {
            id: nextProps.config.id,
            value: nextProps.config.defaultValue
        }
    }

    handleChange(event) {
        this.props.config.handlechange.call(null, this.state.id, event.target.value);
    }

    render() {
        const {classes} = this.props;
        return (<FormControl className={classes.formControl} disabled={this.props.disabled}>
            <TextField key={this.props.config.key}  onChange={this.handleChange.bind(this)} {...this.props.config} className={classes.textField}/>
            <FormHelperText>{this.props.helperText}</FormHelperText>
        </FormControl>)
    }
}

export default withStyles(styles)(DateField);