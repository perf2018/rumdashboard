import React from 'react';
import axios from 'axios';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import Button from '@material-ui/core/Button';
import TextField from 'material-ui/TextField';
import { withStyles } from '@material-ui/core/styles';
import {BrowserRouter as Router, Link, NavLink, Redirect, Prompt} from 'react-router-dom';


class Login extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            username: '',
            password: ''
        }
        this.enableLogin = this.enableLogin.bind(this);
    }

    enableLogin() {
        if (this.state.username && this.state.password) {
            return false;
        }
        return true;
    }

    login(e) {
        e.preventDefault();
        let self = this;
        const { history } = this.props;
        axios.post('http://35.237.44.183:8083/api/users/login', {
            userId: this.state.username,
            password: this.state.password
        }).then(function (response) {
            localStorage.setItem('X-Auth-Token', response.headers['x-auth-token']);
            //history.push('/');
            self.props.reRender();
        }).catch(function (error) {
            self.setState({
                errorMessage: error.message
            })
        });
    }

    render() {
        if(localStorage.getItem('X-Auth-Token')) {
            return <Redirect to="/" />;
        }
        const { classes, theme } = this.props;
        return (<div className={classes.root}>

                <div className={classes.appFrame}>
                    <MuiThemeProvider>
                        <div className={classes.login_wrapper}>
                            <AppBar className={classes.appBar}
                                title="Login"
                            />
                            <div className={classes.login_form}>
                            <TextField
                                id="usernam"
                                autoComplete='off'
                                required={true}
                                hintText="Enter your Username"
                                floatingLabelText="Username"
                                onChange={(event, newValue) => this.setState({username: newValue})}
                            />
                            <br/>
                            <TextField
                                id="pass"
                                autoComplete='off'
                                required={true}
                                type="password"
                                hintText="Enter your Password"
                                floatingLabelText="Password"
                                onChange={(event, newValue) => this.setState({password: newValue})}
                            />
                            <div className='errorField'>{this.state.errorMessage}</div>
                            <br/>
                            <Button disabled={this.enableLogin()} variant="contained" color="primary"
                                    style={style}
                                    onClick={(event) => this.login(event)}>
                                Login
                            </Button>
                            </div>
                        </div>
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }
}

const style = {
    margin: 15,
};

const styles = theme => ({
    appFrame: {
        height: "90%",
        zIndex: 1,
        overflow: 'hidden',
        position: 'relative',
        display: 'flex',
        width: '100%',
    },
    appBar: {
        position: 'absolute',
        transition: theme.transitions.create(['margin', 'width'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        borderBottom:"rgba(0, 0, 0, 0.1) solid 1px",
        backgroundColor:"#EDEDED",
        boxShadow:"none"

    },
    root: {
        flexGrow: 1,
        width: '100%'
    },
    login_form: {
        margin: '120px',
        borderStyle: 'dashed',
        padding: '20px',
    },
    login_wrapper: {
        width: '100%'
    }
})

export default withStyles(styles, { withTheme: true })(Login);