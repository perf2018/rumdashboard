import { MultiLineChart } from "../modules/common/graphs/multi-line-chart";

import PieChartComponent  from "../modules/common/graphs/pie-chart-component";
import Histogram from "../modules/common/graphs/histogram-chart";
import StackedBarChart  from "../modules/common/graphs/stack-bar-chart";
import CustomizedTable  from "../modules/common/graphs/table";

import Map  from "../modules/common/graphs/Map";


import React from 'react';
export default class ChartFactory  extends React.Component{
     
    render() {
        var config = this.props.config;
        config.show = true
        if(!this.props.config.data) {
            return <div></div>
        }
        switch (config.type) {
            case 'MultiLineChart':
                this.chartType = <MultiLineChart data={config}/>;
                break;
            case 'PieChart':
                this.chartType =  <PieChartComponent data={config} />;
                break;
            case 'Histogram':
                this.chartType =  <Histogram data={config} />;
                break;        
            case 'StackedBarChart':
               this.chartType = <StackedBarChart data={config} />;
               break;
            case 'Map' :   
                this.chartType =  <Map data={config}/>;
                break;
            case 'Table' :
                this.chartType =  <CustomizedTable data={config}/>;
                break;
   
            default:
                this.chartType = <div>Given Chart is not available....</div>;
                break;
        }

        return this.chartType;
      
    }
    
}