

/**
 * Custom styling for every chart
 */
export default class ChartProperty {
    constructor(data) {
        this.xdata = data.x || {};
        this.ydata = data.y || {};
        this.ticks = options.ticks || {};
        this.backgroundcolor = options.backgroundcolor || {};
        this.data = data;
    }
    
  
  }